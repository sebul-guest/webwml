#use wml::debian::translation-check translation="e29a0fc272d38a01c5817ee1e8419d80e75e9d30" mindelta="1"
<define-tag description>sikkerhedsopdatering</define-tag>
<define-tag moreinfo>
<p>Flere sårbarheder er opdaget i Linux-kernen, som kunne føre til en 
rettighedsforøgelse, lammelsesangreb eller informationslækager.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-4155">CVE-2021-4155</a>

    <p>Kirill Tkhai opdagede en datalækage i den måde, XFS_IOC_ALLOCSP-IOCTL'en 
    i XFS-filsystemet tillod en størrelsesforøgelse gældende for filer med 
    forskudte størrelser.  En lokal angriber kunne drage nytte af fejlen til at 
    lække data på XFS-filsystemet.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-28711">CVE-2021-28711</a>, 
    <a href="https://security-tracker.debian.org/tracker/CVE-2021-28712">CVE-2021-28712</a>, 
    <a href="https://security-tracker.debian.org/tracker/CVE-2021-28713">CVE-2021-28713</a> (XSA-391)

    <p>Juergen Gross rapporterede at ondsindede PV-backends kunne forårsage et 
    lammelsesangreb til gæster, som serviceres af disse backends gennem 
    højfrekvente events, selv hvis disse backends kører i et mindre priviligeret 
    miljø.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-28714">CVE-2021-28714</a>, 
    <a href="https://security-tracker.debian.org/tracker/CVE-2021-28715">CVE-2021-28715</a> (XSA-392)

    <p>Juergen Gross opdagede at Xen-gæster kunne tvinge Linux' netbackdriver 
    til at lægge beslag på store mængder kernehukommelse, medførende 
    lammelsesangreb.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-39685">CVE-2021-39685</a>

    <p>Szymon Heidrich opdagede en bufferoverløbssårbarhed i 
    USB-gadgetundersystemet, medførende informationsafsløring, lammelsesangreb 
    eller rettighedsforøgelse.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-45095">CVE-2021-45095</a>

    <p>Man opdagede at Phone Network-protokoldriveren (PhoNet) havde en 
    referenceoptællingslækage i funktionen pep_sock_accept().</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-45469">CVE-2021-45469</a>

    <p>Wenqing Liu rapporterede om hukommelsestilgang udenfor grænserne i 
    f2fs-implementeringen, hvis en inode havde en ugyldig sidste 
    xattr-forekomst.  En angriber, der er i stand til at mount'e et særligt 
    fremstillet filaftryk, kunne drage nytte af fejlen til 
    lammelsesangreb.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-45480">CVE-2021-45480</a>

    <p>En hukommelseslækagefejl blev opdaget i funktionen 
    __rds_conn_create() i protokolundersystemet RDS (Reliable Datagram 
    Sockets).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-0185">CVE-2022-0185</a>

    <p>William Liu, Jamie Hill-Daniel, Isaac Badipe, Alec Petridis, Hrvoje
    Misetic og Philip Papurt opdaede en heapbaseret bufferoverløbsfejl i 
    funktionen legacy_parse_param i Filesystem Context-funktionaliteten, hvilket 
    gjorde det muligt for en lokal bruger (med CAP_SYS_ADMIN-muligheden i det 
    aktuelle navnerum) at forøge rettigheder.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-23222">CVE-2022-23222</a>

    <p><q>tr3e</q> opdagede at BPF-verifikatoren ikke på korrekt vis 
    begrænsede flere *_OR_NULL-pointertyper, hvilket tillod at disse typer 
    kunne foretage pointeraritmetik.  En lokal bruger med muliged for at kalde 
    bpf(), kunne drage nytte af fejlen til at forøge rettigheder.  Kald uden 
    rettighed til bpf() er som standard deaktiveret i Debian, hvilket afhjælper 
    fejlen.</p></li>

</ul>

<p>I den stabile distribution (bullseye), er disse problemer rettet i version 
5.10.92-1.  Versionen indeholder ændringer, som det var meningen skulle medtages 
i den næste punktopdatering af Debian bullseye.</p>

<p>Vi anbefaler at du opgraderer dine linux-pakker.</p>

<p>For detaljeret sikkerhedsstatus vedrørende linux, se
dens sikkerhedssporingssidede på:
<a href="https://security-tracker.debian.org/tracker/linux">\
https://security-tracker.debian.org/tracker/linux</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2022/dsa-5050.data"
