#use wml::debian::template title="Le dépôt VCS du projet de documentation Debian (système de contrôle de versions)"
#use wml::debian::translation-check translation="e6e987deba52309df5b062b1567c8f952d7bd476" maintainer="Jean-Pierre Giraud"

# Translator:
# Mickael Simon, 2001-2003
# Frédéric Bothamy, 2005, 2007.
# Simon Paillard, 2008, 2009.
# David Prévot, 2010.
# Jean-Paul Guillonneau, 2018-2021.
# Jean-Pierre Giraud, 2022

<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">

<ul class="toc">
  <li><a href="#access">Accéder aux sources sur le dépôt Git</a></li>
  <li><a href="#obtaining">Obtenir les privilèges de mise à jour</a></li>
  <li><a href="#updates">Mécanisme de mise à jour automatique</a></li>
</ul>

<aside>
<p><span class="fas fa-caret-right fa-3x"></span> Le
<a href="ddp">Projet de documentation Debian</a> (DDP) stocke ses pages web et
la plupart des manuels sur <a href="https://salsa.debian.org">Salsa</a>, une
instance GitLab de Debian. Tout le monde peut télécharger les sources à partir
du service Salsa, mais seuls les membres du projet de documentation Debian y ont
accès en écriture et peuvent mettre à jour les fichiers.</p>
</aside>

<h2><a id="access"></a></h2>

<p>
Le Projet de documentation Debian stocke tous les contenus sur Salsa,
l'instance GitLab de Debian. Pour accéder aux différents fichiers, voir les
modifications récentes et l'activité du projet en général, veuillez visiter le
<a href="https://salsa.debian.org/ddp-team/">dépôt du DDP</a>.
</p>

<p>
Pour télécharger un manuel complet, un accès direct à Salsa est souvent la
meilleure solution. Les paragraphes suivants expliquent comment cloner un dépôt
Git (en lecture seule ou en lecture et écriture) sur votre machine et comment
mettre à jour votre copie locale. Avant de commencer, vous devez installer le
paquet <tt><a href="https://packages.debian.org/git">git</a></tt> sur votre
machine.
</p>

<p style="text-align:center"><button type="button"><span class="fas fa-book-open fa-2x"></span> <a href="https://wiki.debian.org/Salsa">Lire la documentation de Salsa</a></button></p>


<h3>Cloner anonymement un dépôt Git (mode lecture seule)</h3>

<p>
Utilisez cette commande pour télécharger tous les fichiers d’un seul projet :
</p>

<pre>
git clone https://salsa.debian.org/ddp-team/release-notes.git
</pre>

<p>
Faites de même pour chaque projet que vous voulez cloner localement.
<strong>Astuce :</strong> Pour trouver l'URL correcte pour la commande
<code>git clone</code>, ouvrez le projet dans un navigateur web, cliquez sur le
bouton bleu <em>Clone</em> et copiez l'URL <em>Clone with HTTPS</em> dans votre
presse-papier.
</p>

<h3>Cloner un dépôt Git avec les privilèges de mise à jour (mode lecture et écriture)</h3>

<p>
Avant d’accéder au serveur Git en utilisant cette méthode, les droits d’écriture
doivent d’abord vous être accordés. Veuillez lire en premier comment
<a href="#obtaining">solliciter</a> la permission de mise à jour.
</p>

<p>
Avec un accès en écriture pour Salsa, vous pouvez utiliser cette commande pour
télécharger tous les fichiers d’un projet
particulier :
</p>

<pre>
git clone git@salsa.debian.org:ddp-team/release-notes.git
</pre> 

<p>Faites de même pour chaque projet que vous voulez cloner localement.</p>

<h3>Récupérer les modifications du dépôt distant de Git</h3>

<p>
Pour actualiser votre copie locale avec les modifications effectuées par
d'autres personnes, entrez dans les sous-répertoires respectifs du manuel et
lancez la commande :
</p>

<pre>
git pull
</pre>

<h2><a id="obtaining">Obtenir les privilèges de mise à jour</a></h2>

<p>
Les privilèges de mises à jour sont accordés à tous ceux qui désirent participer
à l’écriture des manuels, des FAQ, des guides pratiques, etc. Nous demandons
habituellement que vous ayez d’abord présenté quelques correctifs utiles. Après
cela, suivez ces étapes pour demander l'accès en écriture :
</p>

<ol>
  <li>Créez un compte pour <a href="https://salsa.debian.org/">Salsa</a> si ce n'est pas déjà fait ;</li>
  <li>Allez sur le <a href="https://salsa.debian.org/ddp-team/">dépôt DDP</a> et cliquez sur <em>Request Access</em> ;</li>
  <li>Envoyez un courriel à la liste <a href="mailto:debian-doc@lists.debian.org">debian-doc@lists.debian.org</a> et dites-nous vos antécédents dans le projet Debian ;</li>
  <li>Une fois votre requête acceptée, vous serez membre de l'équipe DDP.</li>
</ol>

<h2><a id="updates">Mécanisme de mise à jour automatique</a></h2>

<p>
Tous les manuels sont publiés comme des pages web générées automatiquement sur
www-master.debian.org dans le cadre du processus de reconstruction régulière du
site web qui se déroule toutes les quatre heures. Pendant ce processus, la
dernière version des paquets est téléchargée de l'archive, chaque manuel est
reconstruit et tous les fichiers sont transférés dans le sous-répertoire
<code>doc/manuals/</code> du site web.
</p>

<p>Les fichiers de la documentation générés par le script de mise à jour sont
disponibles sur <a href="manuals/">https://www.debian.org/doc/manuals/</a>.</p>

<p>Les fichiers des journaux générés par le processus de mise à jour sont
disponibles sur <url "https://www-master.debian.org/build-logs/webwml/" />
(le script s’appelle <code>7doc</code> et est exécuté dans le cadre d’une tâche
cron <code>often</code>).</p>

# <p>Notez que ce processus régénère le répertoire <code>/doc/manuals/</code>
# Le contenu du répertoire <code>/doc/</code> est généré soit depuis
# <a href="/devel/website/desc">webwml</a> soit depuis d'autres scripts,
# comme ceux qui extraient les manuels de certains paquets.</p>
