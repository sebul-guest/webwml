#use wml::debian::translation-check translation="bfffffec31e85b5c768b6090e3d383ff90c78075" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Il existait une vulnérabilité potentielle d'écriture hors limites dans
pixman, une bibliothèque de manipulation de pixels pour de nombreuses
applications graphiques pour Linux.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-44638">CVE-2022-44638</a>

<p>Dans libpixman dans les versions de Pixman antérieures à 0.42.2, il
existait une écriture hors limites (ou dépassement de tas) dans
rasterize_edges_8, due à un dépassement d'entier dans
pixman_sample_floor_y.</p></li>

</ul>

<p>Pour Debian 10 <q>Buster</q>, ce problème a été corrigé dans la version
0.36.0-1+deb10u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets pixman.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3179.data"
# $Id: $
