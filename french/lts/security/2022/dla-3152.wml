#use wml::debian::translation-check translation="d0007529a4b20076323b04bb04837ce8186851c1" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Cette mise à jour corrige une série importante de vulnérabilités dont
une partie significative affecte la conversion de jeux de caractères.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-10228">CVE-2016-10228</a>

<p>Le programme iconv dans la bibliothèque GNU C lorsqu'il est appelé avec
plusieurs suffixes dans l'encodage de destination (TRANSLATE ou IGNORE) en
même temps que l'option -c, entre dans une boucle infinie lors du
traitement de séquences d'entrée multioctets non valables, menant à un
déni de service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-19126">CVE-2019-19126</a>

<p>Dans l'architecture x86-64, la bibliothèque GNU C échoue à ignorer la
variable d'environnement LD_PREFER_MAP_32BIT_EXEC pendant l'exécution du
programme après une transition de sécurité, permettant à des attaquants
locaux de restreindre les adresses de mappage possibles pour les
bibliothèques chargées et ainsi de contourner l'ASLR dans un programme
setuid.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-25013">CVE-2019-25013</a>

<p>La fonction iconv dans la bibliothèque GNU C, peut rencontrer un
dépassement de tampon en écriture lors du traitement de séquences d'entrée
multioctets non valables dans l'encodage EUC-KR.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-10029">CVE-2020-10029</a>

<p>La bibliothèque GNU C pouvait provoquer un dépassement de tampon de pile
pendant une réduction d'intervalle si une entrée pour une fonction à double
précision étendue de 80 bits contient une représentation réelle (<q>bit
pattern</q>) non canonique, comme on le voit avec le passage d'une valeur
0x5d414141414141410000 à sinl sur des cibles x86. Ce problème est lié à
sysdeps/ieee754/ldbl-96/e_rem_pio2l.c.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-1752">CVE-2020-1752</a>

<p>Une vulnérabilité d'utilisation de mémoire après libération introduite
dans glibc a été découverte dans la manière dont le développement du tilde
est réalisé. Les chemins de répertoire contenant un tilde initial suivi par
un nom d'utilisateur valable sont affectés par ce problème. Un attaquant
local pouvait exploiter ce défaut en créant un chemin contrefait pour
l'occasion qui, lors du traitement par la fonction glob, pouvait
éventuellement mener à l'exécution de code arbitraire.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-27618">CVE-2020-27618</a>

<p>La fonction iconv dans la bibliothèque GNU C, lors du traitement de
séquences d'entrée multioctets non valables dans les encodages IBM1364,
IBM1371, IBM1388, IBM1390 et IBM1399, échoue à incrémenter l'état d'entrée,
ce qui pouvait conduire à une boucle infinie dans les applications, avec
pour conséquence un déni de service, une vulnérabilité différente du
<a href="https://security-tracker.debian.org/tracker/CVE-2016-10228">CVE-2016-10228</a>.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-6096">CVE-2020-6096</a>

<p>Une vulnérabilité exploitable de comparaison signée existe dans
l'implémentation de GNU glibc de memcpy() dans ARMv7. Appeler memcpy() (sur
des cibles ARMv7 qui utilisent l'implémentation de GNU glibc) avec une
valeur négative pour le paramètre <q>num</q> a pour conséquence une
vulnérabilité de comparaison signée. Si un attaquant fait déborder par le
bas le paramètre <q>num</q> dans memcpy(), cette vulnérabilité pouvait
conduire à un comportement non défini tel qu'une écriture mémoire hors
limites et éventuellement une exécution de code à distance. En outre, cette
implémentation de memcpy() permet de poursuivre l'exécution d'un programme
dans des scénarios où une erreur de segmentation ou un plantage sont
survenus. Les risques surviennent dans l'exécution qui suit et les
itérations de ce code seront exécutées avec ces données corrompues.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-27645">CVE-2021-27645</a>

<p>Le démon de mise en cache de serveur de noms (nscd) dans la bibliothèque
GNU C, lors du traitement d'une requête de recherche de netgroup, peut
planter du fait d'une double libération de zone de mémoire, avec pour
conséquence éventuellement un service dégradé ou un déni de service à
l'encontre du système local. Ce problème est lié à netgroupcache.c.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-3326">CVE-2021-3326</a>

<p>La fonction iconv dans la bibliothèque GNU C, lors du traitement de
séquences d'entrées non valables dans l'encodage ISO-2022-JP-3, échoue à
réaliser une assertion dans le chemin du code et interrompt le programme,
avec éventuellement pour conséquence un déni de service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-33574">CVE-2021-33574</a>

<p>La fonction mq_notify dans la bibliothèque GNU C a une utilisation de
mémoire après libération. Elle peut utiliser l'objet d'attributs de
processus de notification (passé au moyen de son paramètre de structure
sigevent) après qu'il a été libéré par le processus appelant, menant à un
déni de service (plantage d'application) ou avoir d’autres conséquences
indéterminées.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-35942">CVE-2021-35942</a>

<p>La fonction wordexp dans la bibliothèque GNU C peut planter ou lire une
zone mémoire arbitraire dans parse_param (dans posix/wordexp.c) quand elle
est appelée avec un motif contrefait non sûr, avec pour conséquences
éventuellement un déni de service ou la divulgation d'informations. Cela
survient parce que <q>atoi</q> est utilisé alors que cela aurait dû être
<q>strtoul</q> pour assurer des calculs corrects.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-3999">CVE-2021-3999</a>

<p>Des dépassements de tampon par le haut et pas le bas, dus à un décalage
d'entier dans getcwd() pouvaient conduire à une corruption de mémoire quand
la taille du tampon est de 1 exactement. Un attaquant local qui peut
contrôler le tampon d'entrée et sa taille passés à getcwd() dans un
programme setuid pouvait utiliser ce défaut pour éventuellement exécuter du
code arbitraire et élever ses privilèges sur le système.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-23218">CVE-2022-23218</a>

<p>La fonction de compatibilité obsolète svcunix_create dans le module
sunrpc de la bibliothèque GNU C copie son argument chemin dans la pile sans
valider sa longueur. Cela peut avoir pour conséquence un dépassement de
tampon, aboutissant éventuellement à un déni de service ou (si une
application n'est pas construite avec la protection de pile activée) à
l'exécution de code arbitraire.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-23219">CVE-2022-23219</a>

<p>La fonction de compatibilité obsolète clnt_create dans le module
sunrpc de la bibliothèque GNU C copie son argument nom d'hôte dans la pile
sans valider sa longueur. Cela peut avoir pour conséquence un dépassement
de tampon, aboutissant éventuellement à un déni de service ou (si une
application n'est pas construite avec la protection de pile activée) à
l'exécution de code arbitraire.</p>


<p>Pour Debian 10 Buster, ces problèmes ont été corrigés dans la version
2.28-10+deb10u2.</p>

<p>Nous vous recommandons de mettre à jour vos paquets glibc.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de glibc, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/glibc">\
https://security-tracker.debian.org/tracker/glibc</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p></li>

</ul>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3152.data"
# $Id: $
