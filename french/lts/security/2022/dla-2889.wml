#use wml::debian::translation-check translation="634d967ef0919cff3b91e6e3d821d40bc72a9ee6" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Le projet Drupal inclut une très vieille version de jQuery. Des
vulnérabilités de sécurité menant à des attaques par script intersite ont
été découvertes dans différentes composantes de la bibliothèque jQuery UI
et corrigées pour Drupal version 7.86.</p>

<p>Les correctifs pour les vulnérabilités mentionnées ont été rétroportés
dans la version Debian 9 Stretch (7.52).</p>

<p>Drupal est un riche système de gestion de contenus web ; il a été inclus
dans Debian jusqu'à Stretch, mais n'est plus présent dans les versions
plus récentes. Si vous faites fonctionner un serveur web avec Drupal7, nous
vous recommandons fortement de mettre à niveau le paquet drupal7.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de drupal7, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/drupal7">\
https://security-tracker.debian.org/tracker/drupal7</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>

<p>--AWNW9msBK4+52Jxu
Content-Type: application/pgp-signature; name="signature.asc"</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-2889.data"
# $Id: $
