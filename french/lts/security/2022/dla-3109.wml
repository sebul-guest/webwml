#use wml::debian::translation-check translation="da44eba2ee33944890b7b9b5694f78c59e976f92" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Il a été découvert que des utilisateurs authentifiés pouvaient
déclencher une erreur dans Nova, un contrôleur d'infrastructure
d'informatique dans le nuage, pour provoquer une fuite d'informations.</p>

<p>En complément, cette mise à jour inclut quelques corrections pour la
migration de volume en direct, ainsi que l'ajout d'une URL /healthcheck
pour la supervision.</p>

<p>Pour Debian 10 Buster, ce problème a été corrigé dans la version
2:18.1.0-6+deb10u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets nova.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de nova, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/nova">\
https://security-tracker.debian.org/tracker/nova</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3109.data"
# $Id: $
