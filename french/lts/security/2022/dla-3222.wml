#use wml::debian::translation-check translation="02ee01e13f7668797dcd77af56f634653ab7e4ba" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>ranjit-git a découvert une vulnérabilité de fuite d'informations dans
node-fetch, un module de Node.js exposant une IPA compatible avec window.fetch
dans l’environnement d’exécution de Node.js : le module n’honorait pas la
politique de même origine et ensuite le suivi d’une redirection aboutissait à
une divulgation de cookies pour l’URL cible.</p>

<p>Pour Debian 10 « Buster », ce problème a été corrigé dans
la version 1.7.3-1+deb10u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets node-fetch.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de node-fetch,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/node-fetch">\
https://security-tracker.debian.org/tracker/node-fetch</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3222.data"
# $Id: $
