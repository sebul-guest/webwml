#use wml::debian::translation-check translation="c1ef9113eab29bb183a7873762b3189eec78c7be" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Une vulnérabilité a été découverte dans php-pear qui fournit les paquets
principaux de PEAR (PHP extension et Application Repository). Tar.php dans
Archive_Tar permet des opérations d’écriture avec traversée de répertoires
à cause d’une vérification déficiente de lien symbolique, un problème relatif au
<a href="https://security-tracker.debian.org/tracker/CVE-2020-28948">CVE-2020-28948</a>.
Un attaquant pourrait augmenter ses privilèges en écrasant des fichiers en
dehors du répertoire d’extraction à l’aide d’une archive .tar contrefaite.</p>

<p>Pour Debian 9 <q>Stretch</q>, ce problème a été corrigé dans
la version 1:1.10.1+submodules+notgz-9+deb9u3.</p>

<p>Nous vous recommandons de mettre à jour vos paquets php-pear.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de php-pear, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/php-pear">\
https://security-tracker.debian.org/tracker/php-pear</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2621.data"
# $Id: $
