#use wml::debian::translation-check translation="aa53f615a288bec91ca2ee25c1765e253111ba8b" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Deux problèmes ont été découverts dans Django, le cadriciel de développement
web basé sur Python.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-33203">CVE-2021-33203</a>

<p>Traversée potentielle de répertoires à l'aide d'admindocs</p>

<p>Des membres du personnel pourraient utiliser la vue TemplateDetailView
d’admindocs pour vérifier l’existence de fichiers arbitraires. De plus, si
(et seulement si) les modèles d’admindocs par défaut ont été personnalisés par
les développeurs pour aussi exposer le contenu de fichiers, alors non seulement
l’existence mais le contenu des fichiers est aussi exposé.</p>

<p>Comme mitigation, le nettoyage des chemins est désormais appliqué et seuls
les fichiers dans les répertoires racine de modèles peuvent être chargés.</p>

<p>La sévérité de ce problème est faible selon la politique de sécurité de
Django. Merci à Rasmus Lerchedahl Petersen et Rasmus Wriedt Larsen de l’équipe
CodeQL de Python pour le rapport.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-33571">CVE-2021-33571</a>

<p>Possibles attaques indéterminées SSRF, RFI et LFI puisque les validateurs
acceptent des zéros en tête des adresses IPv4</p>

<p>URLValidator, validate_ipv4_address() et validate_ipv46_address()
n’interdisent pas des zéros en tête de valeur littérale en octal. Si de telles
valeurs sont utilisées, des attaques SSRF, RFI et LFI indéterminées pourraient
être subies.</p>

<p>Les validateurs validate_ipv4_address() et validate_ipv46_address() n’étaient
pas affectés avec Python 3.9.5+.</p>

<p>Ce problème est de sévérité intermédiaire selon la politique de sécurité de
Django.</p>

</ul>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans
la version 1:1.10.7-2+deb9u14.</p>

<p>Nous vous recommandons de mettre à jour vos paquets python-django.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2676.data"
# $Id: $
