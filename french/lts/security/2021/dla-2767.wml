#use wml::debian::translation-check translation="b6fd71038a90e9ea01f69803002751b89c17f38b" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Une vulnérabilité a été découverte dans Apache Santuario, sécurité XML pour
Java : la propriété <q>secureValidation</q> n’est pas passée correctement lors
de la création d’un KeyInfo à partir d’un élément KeyInfoReference. Cela permet
à un attaquant de mal utiliser une transformation d’XPath pour extraire
n’importe quel fichier .xml local dans un élément RetrievalMethod.</p>

<p>Pour Debian 9 « Stretch », ce problème a été corrigé dans
la version 1.5.8-2+deb9u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets libxml-security-java.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de libxml-security-java,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/libxml-security-java">\
https://security-tracker.debian.org/tracker/libxml-security-java</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur : <a rel="nofollow
"href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2767.data"
# $Id: $
