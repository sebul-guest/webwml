#use wml::debian::translation-check translation="bc682112a18cfd06124f29471af7e993dba30524" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Martin Wennberg a découvert que python-ipaddress, un rétroportage du module
ipaddress de Python 3, calculait improprement les valeurs de hachage dans les
classes <code>IPv4Interface</code> et <code>IPv6Interface</code>. Cela pouvait
permettre à un attaquant distant de provoquer un déni de service si une
application était affectée par l’exécution d’un dictionnaire contenant des
objets <code>IPv4Interface</code> ou <code>IPv6Interface</code>. L’attaquant
pouvait utiliser ce défaut pour provoquer la création de beaucoup d’entrées de
dictionnaire.</p>

<p>Pour Debian 10 <q>Buster</q>, ce problème a été corrigé dans
la version 1.0.17-1+deb10u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets python-ipaddress.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de python-ipaddress,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/python-ipaddress">\
https://security-tracker.debian.org/tracker/python-ipaddress</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3424.data"
# $Id: $
