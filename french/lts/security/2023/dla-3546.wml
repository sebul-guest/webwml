#use wml::debian::translation-check translation="c6253c4dedab004700c7166f96f71fb3fe4002ac" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Il a été découvert qu’il existait un problème dans le système de filtrage de
courriels DMARC <tt>opendmarc</tt>. Une vulnérabilité permettait à des
attaquants d’injecter des résultats d’authentification pour fournir de fausses
informations à propos du domaine d’origine du message. Cela était dû à une
analyse et une interprétation incorrectes des résultats d’authentification
SPF/DKIM.</p>


<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-12272">CVE-2020-12272</a>

<p>OpenDMARC jusqu’aux versions 1.3.2 et 1.4.x permettait des attaques
d’injection de résultats d’authentification pour fournir de fausses
informations à propos du domaine d’origine du message. Cela était dû à une
analyse et une interprétation incorrectes des résultats d’authentification
SPF/DKIM, comme le montre la sous-chaine example.net(.example.com.</p></li>

</ul>

<p>Pour Debian 10 <q>Buster</q>, ce problème a été corrigé dans
la version 1.3.2-6+deb10u3.</p>

<p>Nous vous recommandons de mettre à jour vos paquets opendmarc.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3546.data"
# $Id: $
