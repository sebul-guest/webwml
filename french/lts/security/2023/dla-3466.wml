#use wml::debian::translation-check translation="f4835bca1efa23d7142cf0319150fcac732555f2" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Avahi, une implémentation libre zéro configuration (zeroconf) de réseau,
incluant un système pour la découverte de services multidiffusion DNS/DNS-SD,
était sensible à un déni de service. L’évènement utilisé pour signaler la fin
de la connexion du client sur le socket Unix avahi n’était pas géré correctement
dans la fonction client_work, permettant à un attaquant local de déclencher une
boucle infinie.</p>

<p>Pour Debian 10 <q>Buster</q>, ce problème a été corrigé dans
la version 0.7-4+deb10u3.</p>

<p>Nous vous recommandons de mettre à jour vos paquets avahi.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de avahi,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/avahi">\
https://security-tracker.debian.org/tracker/avahi</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3466.data"
# $Id: $
