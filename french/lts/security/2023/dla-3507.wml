#use wml::debian::translation-check translation="9f5fda20ec4fe0292a5ef5e7b6c4c80b0fce3ea2" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Des vulnérabilités d’écriture aléatoire de fichier ont été découvertes dans
pandoc, une bibliothèque Haskell et un outil en ligne de commande pour convertir
d’un format de balisage vers un autre. Ces vulnérabilités pouvaient être
provoquées par la fourniture d’un élément d’image contrefait pour l'occasion en
entrée lors de la génération de fichier en utilisant l’option
<code>--extract-media</code> ou en produisant une sortie au format PDF, et
permettaient à un attaquant de créer ou écraser des fichiers arbitraires du
système (en fonction des privilèges du processus exécutant).</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-35936">CVE-2023-35936</a>

<p>Entroy C a découvert que l’ajout de composants de répertoire avec l’encodage
pourcent à la fin d’un URI malveillant <code>data:</code>, un attaquant pouvait
tromper pandoc pour créer ou écraser des fichiers arbitraires sur le système.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-38745">CVE-2023-38745</a>

<p>Guilhem Moulin a découvert que le correctif amont pour le
<a href="https://security-tracker.debian.org/tracker/CVE-2023-35936">CVE-2023-35936</a>
était incomplet, à savoir que la vulnérabilité demeurait si l’encodage des
caractères « <code>&percnt;</code> » était de la forme
« <code>&percnt;25</code> ».</p></li>

</ul>

<p>Pour Debian 10 <q>Buster</q>, ces problèmes ont été corrigés dans
la version 2.2.1-3+deb10u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets pandoc.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de pandoc,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/pandoc">\
https://security-tracker.debian.org/tracker/pandoc</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3507.data"
# $Id: $
