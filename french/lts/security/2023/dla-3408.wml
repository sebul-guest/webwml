#use wml::debian::translation-check translation="ab7753838f9eed067afe04c2fde3d65ef72a6fe3" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été corrigées dans JRuby, une implémentation
en Java du langage de programmation Ruby.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-17742">CVE-2017-17742</a>
<a href="https://security-tracker.debian.org/tracker/CVE-2019-16254">CVE-2019-16254</a>

<p>Attaques par fractionnement de réponse HTTP dans le serveur HTTP de WEBrick.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-16201">CVE-2019-16201</a>

<p>Vulnérabilité de déni de service par expression rationnelle dans
l’authentification d’accès de Digest de WEBrick.
</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-16255">CVE-2019-16255</a>

<p>Vulnérabilité d’injection de code.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-25613">CVE-2020-25613</a>

<p>Attaque par dissimulation de requête HTTP dans WEBrick.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-31810">CVE-2021-31810</a>

<p>Vulnérabilité de fiabilité de réponses FTP PASV dans Net::FTP.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-32066">CVE-2021-32066</a>

<p>Pas de levée d’exception par Net::IMAP quand StartTLS échouait avec une
réponse inconnue.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-28755">CVE-2023-28755</a>

<p>Retour quadratique sur trace pour un URI non valable.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-28756">CVE-2023-28756</a>

<p>Mauvaise gestion par l’analyseur Time de chaines non autorisées ayant des
caractères spécifiques.</p></li>

</ul>

<p>Pour Debian 10 <q>Buster</q>, ces problèmes ont été corrigés dans
la version 9.1.17.0-3+deb10u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets jruby.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de jruby,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/jruby">\
https://security-tracker.debian.org/tracker/jruby</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3408.data"
# $Id: $
