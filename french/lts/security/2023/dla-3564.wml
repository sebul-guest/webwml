#use wml::debian::translation-check translation="6089069b3a48c6e0b31c55455d11c4afacee80a3" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Il a été découvert qu’il existait une vulnérabilité potentielle d’homme du
milieu (Man dans the Middle – MITM) dans e2guardian, un moteur de filtrage de
contenu web.</p>

<p>Une validation de certificats SSL manquait dans le moteur de prévention MITM
d’e2guardian. Dans le mode autonome (c’est-à-dire agissant comme mandataire ou
mandataire transparent) avec MITM SSL activé, e2guardian ne validait pas les
noms d’hôte dans les certificats de serveurs web auxquels il était connecté et,
par conséquent, était lui-même vulnérable à des attaques MITM.</p>

<ul>
<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-44273">CVE-2021-44273</a>
<p>e2guardian versions v5.4.x &lt;= 5.4.3r était affecté par des validations
manquantes de certificats SSL dans le moteur MITM SSL. Dans le mode autonome,
(c’est-à-dire agissant comme mandataire ou mandataire transparent) avec MITM
SSL activé, e2guardian, si construit avec OpenSSL version 1.1.x, ne validait pas
les noms d’hôte dans les certificats de serveurs web auxquels il était connecté
et, par conséquent, était lui-même vulnérable à des attaques MITM.</p></li>


</ul>

<p>Pour Debian 10 <q>Buster</q>, ce problème a été corrigé dans
la version 5.3.1-1+deb10u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets e2guardian.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3564.data"
# $Id: $
