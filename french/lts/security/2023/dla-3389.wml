#use wml::debian::translation-check translation="345aee3bdc2b1eb18d0e9bc557e4638fceccf84a" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Il a été découvert qu’il existait deux attaques par déni de service (DoS)
potentielles dans lldpd, une implémentation du protocole IEEE 802.1ab (LLDP)
utilisé pour administrer et superviser des périphériques réseau.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-27827">CVE-2020-27827</a>

<p>Un défaut a été découvert dans plusieurs versions d’OpenvSwitch. Des paquets
LLDP contrefaits pour l'occasion pouvaient provoquer une perte de mémoire lors
de l’allocation de données pour gérer des TLV spécifiques optionnels, causant
éventuellement un déni de service. Le plus grand danger de cette vulnérabilité
concernait la disponibilité du système.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-43612">CVE-2021-43612</a>

<p>Plantage dans le décodeur SONMP.</p></li>

</ul>

<p>Pour Debian 10 <q>Buster</q>, ces problèmes ont été corrigés dans
la version 1.0.3-1+deb10u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets lldpd.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3389.data"
# $Id: $
