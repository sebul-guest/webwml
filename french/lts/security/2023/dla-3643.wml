#use wml::debian::translation-check translation="8eea52177a40cb3e506179e8fbd82f4073045960" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Il a été découvert qu’il existait une vulnérabilité d’écrasement de fichier
arbitraire dans pmix, une bibliothèque utilisée dans le calcul en parallèle ou
dans des grappes. Des attaquants pouvaient devenir propriétaires de fichiers
arbitraires à l'aide d'une situation de compétition relative à un lien
symbolique lors de l’exécution de code de bibliothèque avec l’UID 0.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-41915">CVE-2023-41915</a>

<p>OpenPMIx PMIx avant les versions 4.2.6 et 5.0.x avant 5.0.1 permettait à des
attaquants de devenir propriétaires de fichiers arbitraires à l'aide d'une
situation de compétition lors de l’exécution de code de bibliothèque avec
l’UID 0.</p></li>

</ul>

<p>Pour Debian 10 <q>Buster</q>, ce problème a été corrigé dans
la version 3.1.2-3+deb10u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets pmix.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3643.data"
# $Id: $
