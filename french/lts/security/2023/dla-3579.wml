#use wml::debian::translation-check translation="7e15fe107a0e1f6a32da3a93a3626d9646a482a5" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Un problème a été découvert dans elfutils, une collection d’utilitaires pour
gérer les objets ELF. À cause de vérifications de limites et d’assertions
disponibles manquantes, un attaquant pouvait utiliser des fichiers elf
contrefaits pour déclencher un plantage d’application amenant un déni de
service.</p>

<p>Pour Debian 10 <q>Buster</q>, ce problème a été corrigé dans
la version 0.176-1.1+deb10u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets elfutils.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de elfutils,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/elfutils">\
https://security-tracker.debian.org/tracker/elfutils</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3579.data"
# $Id: $
