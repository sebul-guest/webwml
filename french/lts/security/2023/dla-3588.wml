#use wml::debian::translation-check translation="dd1b895d316234369e83d7d9b6c4d60a7e7a5700" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été trouvées dans l’éditeur de texte vim.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-4752">CVE-2023-4752</a>

<p>Une utilisation de mémoire de tas après libération a été découverte dans
 ins_compl_get_exp().</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-4781">CVE-2023-4781</a>

<p>Un dépassement de tampon de tas a été découvert dans vim_regsub_both().</p></li>

</ul>

<p>Pour Debian 10 <q>Buster</q>, ces problèmes ont été corrigés dans
la version 2:8.1.0875-5+deb10u6.</p>

<p>Nous vous recommandons de mettre à jour vos paquets vim.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de vim,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/vim">\
https://security-tracker.debian.org/tracker/vim</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3588.data"
# $Id: $
