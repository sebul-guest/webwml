#use wml::debian::translation-check translation="f5ba2570d9872a3847251af950e0dc549aa011bb" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités de sécurité ont été découvertes dans le moteur de
servlets et JSP Tomcat.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-24998">CVE-2023-24998</a>

<p>Déni de service. Tomcat utilise une copie empaquetée renommée d’Apache
Commons FileUpload pour fournir la fonctionnalité de téléversement de fichier
définie dans la spécification de servlets Jakarta. Apache Tomcat était par
conséquent aussi sensible à la vulnérabilité Commons FileUpload
<a href="https://security-tracker.debian.org/tracker/CVE-2023-24998">CVE-2023-24998</a>
car il n’existait pas de limite au nombre de parties de requête traitées. Cela
rendait possible à un attaquant le déclenchement d’un déni de service à l’aide
d’un téléversement malveillant ou d’une série de téléversements.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-41080">CVE-2023-41080</a>

<p>Redirection ouverte. Si l’application web ROOT (par défaut) était configurée
pour utiliser l’authentification FORM, alors il était possible qu’une URL
contrefaite pour l'occasion soit utilisée pour déclencher une redirection vers une
URL choisie par l’attaquant.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-42795">CVE-2023-42795</a>

<p>Divulgation d'informations. Lors du recyclage de divers objets internes,
incluant la requête et la réponse, avant une réutilisation par les prochaines
requête/réponse, une erreur pouvait faire que Tomcat omette certaines parties
du processus de recyclage, conduisant à une fuite d'informations de la
requête/réponse en cours vers la prochaine.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-44487">CVE-2023-44487</a>

<p>Déni de service provoqué par une surcharge de trame HTTP/2 (Rapid Reset
Attack)</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-45648">CVE-2023-45648</a>

<p>Trafic de requête. Tomcat n’analysait pas correctement les en-têtes
<q>trailer</q> HTTP. Un <q>trailer</q> contrefait pour l'occasion et non valable
pouvait faire que Tomcat traite une seule requête comme plusieurs requêtes,
conduisant à une possibilité de trafic de requête derrière un mandataire
inverse.</p></li>

</ul>

<p>Pour Debian 10 <q>Buster</q>, ces problèmes ont été corrigés dans
la version 9.0.31-1~deb10u9.</p>

<p>Nous vous recommandons de mettre à jour vos paquets tomcat9.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de tomcat9,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/tomcat9">\
https://security-tracker.debian.org/tracker/tomcat9</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur : <a rel="nofollow
"href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3617.data"
# $Id: $
