#use wml::debian::translation-check translation="529ad37ef7b587953f9f07c7751f2de171e943e3" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Il a été découvert qu’il existait un script intersite (XSS) potentiel dans
ruby-sanitize, un nettoyeur HTML basé sur une liste blanche.</p>

<p>En utilisant une entrée soigneusement contrefaite, un attaquant pouvait
dissimuler du HTML et CSS arbitraires à travers Sanitize lorsque qu’il était
configuré pour utiliser la configuration interne <code>relaxed</code> ou en
utilisant une configuration personnalisée qui permettaient des éléments
<code>style</code> et une ou plus de règles <q>at</q>. Cela pouvait aboutir à un
script intersite (XSS) ou à un autre comportement indésiré si du HTML et CSS
étaient rendus dans un navigateur.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-36823">CVE-2023-36823</a>

<p>Sanitize est un nettoyeur HTML et CSS basé sur une liste blanche. En
utilisant une entrée soigneusement contrefaite, un attaquant pouvait dissimuler
du HTML et CSS arbitraires à travers Sanitize, depuis la version 3.0.0 et avant
la version 6.0.2, quand celui-ci était configuré pour utiliser la configuration
interne <q>relaxed</q> ou une configuration personnalisée qui permettait un ou
plus d’éléments <q>style</q> et une ou plus de règles <q>at</q>. Cela pouvait
aboutir à un script intersite ou à un autre comportement non désiré quand du
HTML et CSS malveillants étaient rendus dans un navigateur. Sanitize 6.0.2
réalise une protection supplémentaire de CSS dans le contenu des éléments
<q>style</q>, qui corrige ce problème.</p></li>

</ul>

<p>Pour Debian 10 <q>Buster</q>, ce problème a été corrigé dans
la version 4.6.6-2.1~deb10u2.</p>

<p>Nous vous recommandons de mettre à jour vos paquets ruby-sanitize.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3652.data"
# $Id: $
