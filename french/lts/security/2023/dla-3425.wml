#use wml::debian::translation-check translation="becc85eb30ec52efd938d01b53dfd30a4413a762" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Erik Krogh Kristensen a découvert que sqlparse, un analyseur SQL non
validant, contenait une expression rationnelle qui était vulnérable à une
attaque ReDoS (Regular Expression Denial of Service).</p>

<p>Pour Debian 10 <q>Buster</q>, ce problème a été corrigé dans
la version 0.2.4-1+deb10u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets sqlparse.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de sqlparse,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/sqlparse">\
https://security-tracker.debian.org/tracker/sqlparse</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3425.data"
# $Id: $
