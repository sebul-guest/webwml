#use wml::debian::translation-check translation="ab3054f77f3bc21eeef2d2d1a47a7928f3559a74" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été corrigées dans l’analyseur de trafic réseau
Wireshark.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-1161">CVE-2023-1161</a>

<p>Plantage du dissecteur ISO 15765</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-1992">CVE-2023-1992</a>

<p>Plantage du dissecteur RPCoRDMA</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-1993">CVE-2023-1993</a>

<p>Vulnérabilité de large boucle du dissecteur LISP</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-1994">CVE-2023-1994</a>

<p>Plantage du dissecteur GQUIC</p></li>

</ul>

<p>Pour Debian 10 <q>Buster</q>, ces problèmes ont été corrigés dans
la version 2.6.20-0+deb10u6.</p>

<p>Nous vous recommandons de mettre à jour vos paquets wireshark.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de wireshark,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/wireshark">\
https://security-tracker.debian.org/tracker/wireshark</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3402.data"
# $Id: $
