#use wml::debian::translation-check translation="06125b6cf9c8f0bec9e8b2a6c4d3a96d54d85022" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Une vulnérabilité potentielle de script intersite (XSS) a été découverte dans
rails, un cadriciel MVC basé sur Ruby. Des vues permettant à l’utilisateur de
contrôler la valeur par défaut (non obtenue) des assistants <q>t</q> et
<q>translate</q> seraient susceptibles à des attaques XSS. Lorsqu’une chaîne
non sûre du point de vue HTML est fournie comme valeur par défaut pour une clé
de traduction manquante appelée html ou se terminant par _html, la chaîne par
défaut est marquée sûre du point de vue HTML et n’est pas protégée.</p>

<p>Pour Debian 9 <q>Stretch</q>, ce problème a été corrigé dans
la version 2:4.2.7.1-1+deb9u4.</p>

<p>Nous vous recommandons de mettre à jour vos paquets rails.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de rails, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/rails">https://security-tracker.debian.org/tracker/rails</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a rel="nofollow
"href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2403.data"
# $Id: $
