#use wml::debian::translation-check translation="64102dcb0991f580c6515da364c20efeca52ee5d" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Deux vulnérabilités ont été découvertes dans mutt, un client de courriel en
console.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-14093">CVE-2020-14093</a>

<p>Mutt permettait une attaque de type « homme du milieu » sur fcc/postpone
d’IMAP à l'aide d'une réponse PREAUTH.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-14954">CVE-2020-14954</a>

<p>Mutt possédait un problème de mise en tampon de STARTTLS qui affectait IMAP,
SMTP et POP3. Lorsque le serveur avait envoyé une réponse <q>begin TLS</q>, le
client lisait des données supplémentaires (par exemple, à partir d’une attaque
d’homme du milieu) et les évaluait dans un contexte TLS, c'est-à-dire « response
injection ».</p></li>

</ul>

<p>Dans Jessie de Debian, le paquet source mutt construit deux variantes de mutt :
mutt et mutt-patched.</p>

<p>La précédente version du paquet (1.5.23-3+deb8u2, DLA-2268-1) fournissait des
correctifs pour les problèmes cités ci-dessus, mais étaient appliqués uniquement
sur la construction du paquet mutt-patched, mais pas sur la construction du
paquet mutt (vanilla).</p>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans
la version 1.5.23-3+deb8u3.</p>

<p>Nous vous recommandons de mettre à jour vos paquets mutt.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2268-2.data"
# $Id: $
