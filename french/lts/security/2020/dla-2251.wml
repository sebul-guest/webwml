#use wml::debian::translation-check translation="3263a88b9236350e8abab16cabd24c004e2aabc6" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Deux vulnérabilités ont été trouvées dans Ruby on Rails, un cadriciel MVC
basé sur Ruby adapté au développement d’applications web, qui pourraient
conduire à une exécution de code à distance et l’utilisation de saisies
utilisateur, selon l’application.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-8164">CVE-2020-8164</a>

<p>Vecteur de contournement de paramètres forts dans ActionPack. Dans certains
cas, l’information fournie par l’utilisateur pourrait être divulguée par
inadvertance à partir des paramètres forts. Particulièrement, la valeur renvoyée
par « each », « each_value » ou « each_pair » renverrait le hachage de données
sous-jacentes <q>non fiables</q> qui était lu pour ces paramètres. Les 
applications qui utilisent cette valeur de retour pourraient involontairement
utiliser des entrées utilisateur non fiables.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-8165">CVE-2020-8165</a>

<p>Déconversion potentielle inattendue d’objets fournis par l’utilisateur dans
MemCacheStore. Il existait éventuellement un comportement inattendu dans
MemCacheStore où, lorsque des entrées utilisateur non fiables étaient écrites
dans le stockage de cache en utilisant le paramètre « raw: true », la relecture
du résultat à partir du cache pouvait juger la saisie d’utilisateur comme un
objet déconverti au lieu d’un texte simple. La déconversion d’une saisie
utilisateur non fiable pouvait avoir un impact pouvant aller jusqu’à une 
exécution de code à distance. Au minimum, cette vulnérabilité permettait à un
attaquant d’injecter des objets Ruby dans une application web.</p>

<p>En plus de la mise à niveau vers la dernière version de Rails, les
développeurs doivent faire en sorte que chaque fois qu’ils appellent
« Rails.cache.fetch », il utilisent des valeurs cohérentes du paramètre « raw »
pour la lecture comme l’écriture.</p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans
la version 2:4.1.8-1+deb8u7.</p>

<p>Nous vous recommandons de mettre à jour vos paquets rails.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2251.data"
# $Id: $
