#use wml::debian::translation-check translation="b634d45e05c52fff0d11e083b549f5dad5ec4600" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité</define-tag>
<define-tag moreinfo>
<p>Une régression a été découverte dans la classe Http2UpgradeHandler de
Tomcat 9, introduite dans le correctif pour traiter le
<a href="https://security-tracker.debian.org/tracker/CVE-2023-44487">CVE-2023-44487</a>
(attaque de type <q>Rapid Reset</q>). Une valeur incorrecte de la variable
overheadcount faisait se terminer prématurément les connexions HTTP2.</p>

<p>Pour la distribution oldstable (Bullseye), ce problème a été corrigé
dans la version 9.0.43-2~deb11u9.</p>

<p>Nous vous recommandons de mettre à jour vos paquets tomcat9.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de tomcat9, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/tomcat9">\
https://security-tracker.debian.org/tracker/tomcat9</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2023/dsa-5522.data"
# $Id: $
