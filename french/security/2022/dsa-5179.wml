#use wml::debian::translation-check translation="435683d1174de7bc10428da6d5d629b10af1ce9e" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité</define-tag>
<define-tag moreinfo>
<p>Charles Fol a découvert deux problèmes de sécurité dans PHP, un langage
de script généraliste à source libre couramment utilisé, qui pourraient
avoir pour conséquence un déni de service ou éventuellement l'exécution de
code arbitraire :</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-31625">CVE-2022-31625</a>

<p>Un traitement incorrect de la mémoire dans la fonction pg_query_params().</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-31626">CVE-2022-31626</a>

<p>Un dépassement de tampon dans l'extension mysqld.</p></li>

</ul>

<p>Pour la distribution stable (Bullseye), ces problèmes ont été corrigés
dans la version 7.4.30-1+deb11u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets php7.4.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de php7.4, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/php7.4">\
https://security-tracker.debian.org/tracker/php7.4</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2022/dsa-5179.data"
# $Id: $
