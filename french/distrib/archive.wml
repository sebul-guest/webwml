#use wml::debian::template title="Archive de la distribution"
#use wml::debian::translation-check translation="35754139a03a014558f50e940f9e2ce678c59dfe" maintainer="Jean-Paul Guillonneau"
#use wml::debian::toc

<toc-display />

<toc-add-entry name="old-archive">debian-archive</toc-add-entry>

<p>Si vous avez besoin d'accéder à l'une des anciennes distributions de
Debian, vous pouvez les trouver dans les <a
href="https://archive.debian.org/debian/">archives Debian</a>,
<tt>https://archive.debian.org/debian/</tt>.</p>

<p>Les versions sont stockées sous leur nom de code dans le
répertoire dists/.</p>

<ul>
  <li><a href="../releases/stretch/">Stretch</a> est la Debian 9</li>
  <li><a href="../releases/jessie/">Jessie</a> est la Debian 8.0</li>
  <li><a href="../releases/wheezy/">Wheezy</a> est la Debian 7.0</li>
  <li><a href="../releases/squeeze/">Squeeze</a> est la Debian 6.0</li>
  <li><a href="../releases/lenny/">Lenny</a> est la Debian 5.0</li>
  <li><a href="../releases/etch/">Etch</a> est la Debian 4.0</li>
  <li><a href="../releases/sarge/">Sarge</a> est la Debian 3.1</li>
  <li><a href="../releases/woody/">Woody</a> est la Debian 3.0</li>
  <li><a href="../releases/potato/">Potato</a> est la Debian 2.2</li>
  <li><a href="../releases/slink/">Slink</a> est la Debian 2.1</li>
  <li><a href="../releases/hamm/">Hamm</a> est la Debian 2.0</li>
  <li>Bo est la Debian 1.3</li>
  <li>Rex est la Debian 1.2</li>
  <li>Buzz est la Debian 1.1</li>
</ul>

<p>Nous conservons uniquement le code source pour les publications postérieures
à Bo ainsi que les binaires et le code source pour Bo et les publications plus
récentes. Au fur et à mesure que le temps passe, nous supprimons les paquets
binaires des anciennes versions.

<p>Si vous utilisez APT, vous pouvez mettre par exemple les lignes
suivantes dans votre fichier sources.list :</p>

<pre>
  deb http://archive.debian.org/debian/ hamm contrib main non-free
</pre>
<p>ou</p>
<pre>
  deb http://archive.debian.org/debian/ bo bo-unstable contrib main non-free
</pre>

<p>La liste suivante est une liste des miroirs qui contiennent l'archive :</p>

#include "$(ENGLISHDIR)/distrib/archive.mirrors"
<archivemirrors>

<toc-add-entry name="non-us-archive">Archive non-US</toc-add-entry>

<p>Auparavant, certains logiciels empaquetés pour Debian ne pouvaient pas être
distribués aux États-Unis (ainsi que dans d'autres pays) en raison de
restrictions sur l'exportation de procédés de chiffrement ou de brevets
logiciels. Debian maintenait une archive spéciale appelée <em>non-US</em>.</p>

<p>Ces paquets ont été incorporés à l'archive principale de Debian 3.1 et
l'archive non-US a été arrêtée. Celle-ci fait désormais
partie des archives de archive.debian.org.</p>

<p>Ces paquets sont encore disponibles sur la machine archive.debian.org.
Les méthodes d'accès disponibles sont :</p>
<blockquote><p>
<a href="https://archive.debian.org/debian-non-US/">https://archive.debian.org/debian-non-US/</a><br>
rsync://archive.debian.org/debian-non-US/  (limité)
</p></blockquote>

<p>Pour utiliser ces paquets avec APT, il est nécessaire d'ajouter à
sources.list les entrées suivantes du type :</p>

<pre>
  deb http://archive.debian.org/debian-non-US/ woody/non-US main contrib non-free
  deb-src http://archive.debian.org/debian-non-US/ woody/non-US main contrib non-free
</pre>
