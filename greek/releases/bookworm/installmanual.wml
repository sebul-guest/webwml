#use wml::debian::template title="Debian bookworm -- Οδηγός Εγκατάστασης" BARETITLE=true
#use wml::debian::release
#include "$(ENGLISHDIR)/releases/info"
#include "$(ENGLISHDIR)/releases/arches.data"
#include "$(ENGLISHDIR)/releases/bookworm/release.data"
#use wml::debian::translation-check translation="4706eea1252cf93d1277b69734c50f693a1f5e8b" maintainer="galaxico"

<if-stable-release release="buster">
<p>This is a <strong>beta version</strong> of the Installation Guide for Debian
11, codename bullseye, which isn't released yet. The information
presented here might be outdated and/or incorrect because of changes to
the installer. You might be interested in the
<a href="../buster/installmanual">Installation Guide for Debian
10, codename buster</a>, which is the latest released version of
Debian; or in the <a href="https://d-i.debian.org/manual/">Development
version of the Installation Guide</a>, which is the most up-to-date version
of this document.</p>
</if-stable-release>

<if-stable-release release="bullseye">
<p>Αυτή είναι μια <strong>έκδοση beta</strong> του Οδηγού Εγκατάστασης για το Debian
12, με την κωδική ονομασία bookworm, που δεν έχει ακόμα κυκλοφορήσει. Οι πληροφορίες 
που παρουσιάζονται εδώ μπορεί να είναι ξεπερασμένης ή/και λανθασμένες εξαιτίας αλλαγών
στον εγκαταστάτη. Ίσως ενδιαφέρεστε για τον 
<a href="../bullseye/installmanual">Οδηγό Εγκατάστασης για το Debian
11, με την κωδική ονομασία bullseye</a>, που είναι η έκδοση του Debian
που κυκλοφόρησε πιο πρόσφατα· ή για την <a href="https://d-i.debian.org/manual/">Υπό ανάπτυξη 
έκδοση του Οδηγού Εγκατάστασης</a>, που είναι η πιο ενημερωμένη έκδοση αυτού του κειμένου.</p>
</if-stable-release>

<p>Οδηγίες εγκατάστασης, μαζί με μεταφορτώσιμα αρχεία, είναι διαθέσιμα για κάθε μία από 
τις υποστηριζόμενες αρχιτεκτονικές:</p>

<ul>
<:= &permute_as_list('', 'Οδηγός Εγκατάστασης'); :>
</ul>

<p>Αν έχετε ρυθμίσει σωστά την τοπικοποίηση του περιηγητή ιστοσελίδων, 
μπορείτε να χρησιμοποιήσετε τους παραπάνω συνδέσμους για να έχετε αυτόματα την σωστή HTML έκδοση 
για τη γλώσσα σας &mdash; δείτε την <a href="$(HOME)/intro/cn">διαπραγμάτευση περιεχομένου</a>.
Διαφορετικά, επιλέξτε την ακριβή αρχιτεκτονική, γλώσσα και μορφοποίηση που θέλετε από τον
πίνακα που ακολουθεί.</p>

<div class="centerdiv">
<table class="reltable">
<tr>
  <th align="left"><strong>Αρχιτεκτονική</strong></th>
  <th align="left"><strong>Μορφοποίηση</strong></th>
  <th align="left"><strong>Γλώσσες</strong></th>
</tr>
<: &permute_as_matrix_new( file => 'install', langs => \%langsinstall,
			   formats => \%formats, arches => \@arches,
			   html_file => 'index', namingscheme => sub {
			   "$_[0].$_[1].$_[2]" } ); :>
</table>
</div>
