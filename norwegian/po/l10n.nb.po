msgid ""
msgstr ""
"Project-Id-Version: Debian webwml organization\n"
"PO-Revision-Date: 2008-10-01 19:47+0200\n"
"Last-Translator: Hans Fredrik Nordhaug <hans@nordhaug.priv.no>\n"
"Language-Team: Norwegian Bokmål <i18n-nb@lister.ping.uio.no>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../english/international/l10n/dtc.def:10
msgid "File"
msgstr "Fil"

#: ../../english/international/l10n/dtc.def:14
msgid "Package"
msgstr "Pakke"

#: ../../english/international/l10n/dtc.def:18
msgid "Score"
msgstr "Poeng"

#: ../../english/international/l10n/dtc.def:22
msgid "Translator"
msgstr "Oversetter"

#: ../../english/international/l10n/dtc.def:26
msgid "Team"
msgstr "Lag"

#: ../../english/international/l10n/dtc.def:30
msgid "Date"
msgstr "Dato"

#: ../../english/international/l10n/dtc.def:34
msgid "Status"
msgstr "Status"

#: ../../english/international/l10n/dtc.def:38
msgid "Strings"
msgstr "Strenger"

#: ../../english/international/l10n/dtc.def:42
msgid "Bug"
msgstr "Feil"

#: ../../english/international/l10n/dtc.def:49
msgid "<get-var lang />, as spoken in <get-var country />"
msgstr "<get-var lang />, som det snakkes i <get-var country />"

#: ../../english/international/l10n/dtc.def:54
msgid "Unknown language"
msgstr "Ukjent språk"

#: ../../english/international/l10n/dtc.def:64
msgid "This page was generated with data collected on: <get-var date />."
msgstr "Denne siden ble generert med data samlet: <get-var date />."

#: ../../english/international/l10n/dtc.def:69
msgid "Before working on these files, make sure they are up to date!"
msgstr "Vær sikker på at disse filere er oppdatert før du jobber med dem."

#: ../../english/international/l10n/dtc.def:79
msgid "Section: <get-var name />"
msgstr "Seksjon: <get-var name />"

#: ../../english/international/l10n/menu.def:10
msgid "L10n"
msgstr "L10n"

#: ../../english/international/l10n/menu.def:14
msgid "Language list"
msgstr "Liste over språk"

#: ../../english/international/l10n/menu.def:18
msgid "Ranking"
msgstr "Rangering"

#: ../../english/international/l10n/menu.def:22
msgid "Hints"
msgstr "Tips"

#: ../../english/international/l10n/menu.def:26
msgid "Errors"
msgstr "Feil"

#: ../../english/international/l10n/menu.def:30
msgid "POT files"
msgstr "POT-filer"

#: ../../english/international/l10n/menu.def:34
msgid "Hints for translators"
msgstr "Tips for oversettere"
