msgid ""
msgstr ""
"Project-Id-Version: \n"
"POT-Creation-Date: \n"
"PO-Revision-Date: 2022-05-25 11:00+0000\n"
"Last-Translator: Med <medeb@protonmail.com>\n"
"Language-Team: Arabic <debian-l10n-arabic@lists.debian.org>\n"
"Language: ar\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=6; plural=n==0 ? 0 : n==1 ? 1 : n==2 ? 2 : n%100>=3 "
"&& n%100<=10 ? 3 : n%100>=11 ? 4 : 5;\n"
"X-Generator: Poedit 2.4.2\n"

#: ../../english/Bugs/pkgreport-opts.inc:17
msgid "in package"
msgstr "في الحزمة"

#: ../../english/Bugs/pkgreport-opts.inc:20
#: ../../english/Bugs/pkgreport-opts.inc:60
#: ../../english/Bugs/pkgreport-opts.inc:94
msgid "tagged"
msgstr "موسوم"

#: ../../english/Bugs/pkgreport-opts.inc:23
#: ../../english/Bugs/pkgreport-opts.inc:63
#: ../../english/Bugs/pkgreport-opts.inc:97
msgid "with severity"
msgstr "بالشدة"

#: ../../english/Bugs/pkgreport-opts.inc:26
msgid "in source package"
msgstr "في الحزمة المصدرية"

#: ../../english/Bugs/pkgreport-opts.inc:29
msgid "in packages maintained by"
msgstr "في الحزم المصانة من قِبل"

#: ../../english/Bugs/pkgreport-opts.inc:32
msgid "submitted by"
msgstr "المرسلة من قِبل"

#: ../../english/Bugs/pkgreport-opts.inc:35
msgid "owned by"
msgstr "المملوكة لـ"

#: ../../english/Bugs/pkgreport-opts.inc:38
msgid "with status"
msgstr "في الحالة"

#: ../../english/Bugs/pkgreport-opts.inc:41
msgid "with mail from"
msgstr ""

#: ../../english/Bugs/pkgreport-opts.inc:44
msgid "newest bugs"
msgstr ""

#: ../../english/Bugs/pkgreport-opts.inc:57
#: ../../english/Bugs/pkgreport-opts.inc:91
msgid "with subject containing"
msgstr ""

#: ../../english/Bugs/pkgreport-opts.inc:66
#: ../../english/Bugs/pkgreport-opts.inc:100
msgid "with pending state"
msgstr ""

#: ../../english/Bugs/pkgreport-opts.inc:69
#: ../../english/Bugs/pkgreport-opts.inc:103
msgid "with submitter containing"
msgstr ""

#: ../../english/Bugs/pkgreport-opts.inc:72
#: ../../english/Bugs/pkgreport-opts.inc:106
msgid "with forwarded containing"
msgstr ""

#: ../../english/Bugs/pkgreport-opts.inc:75
#: ../../english/Bugs/pkgreport-opts.inc:109
msgid "with owner containing"
msgstr ""

#: ../../english/Bugs/pkgreport-opts.inc:78
#: ../../english/Bugs/pkgreport-opts.inc:112
msgid "with package"
msgstr ""

#: ../../english/Bugs/pkgreport-opts.inc:122
msgid "normal"
msgstr "عادية"

#: ../../english/Bugs/pkgreport-opts.inc:125
msgid "oldview"
msgstr ""

#: ../../english/Bugs/pkgreport-opts.inc:128
msgid "raw"
msgstr "خام"

#: ../../english/Bugs/pkgreport-opts.inc:131
msgid "age"
msgstr ""

#: ../../english/Bugs/pkgreport-opts.inc:137
msgid "Repeat Merged"
msgstr ""

#: ../../english/Bugs/pkgreport-opts.inc:138
msgid "Reverse Bugs"
msgstr ""

#: ../../english/Bugs/pkgreport-opts.inc:139
msgid "Reverse Pending"
msgstr ""

#: ../../english/Bugs/pkgreport-opts.inc:140
msgid "Reverse Severity"
msgstr ""

#: ../../english/Bugs/pkgreport-opts.inc:141
msgid "No Bugs which affect packages"
msgstr ""

#: ../../english/Bugs/pkgreport-opts.inc:143
msgid "None"
msgstr "لاشيء"

#: ../../english/Bugs/pkgreport-opts.inc:144
msgid "testing"
msgstr "اختبارية"

#: ../../english/Bugs/pkgreport-opts.inc:145
msgid "oldstable"
msgstr "مستقرة قديمة"

#: ../../english/Bugs/pkgreport-opts.inc:146
msgid "stable"
msgstr "مستقرة"

#: ../../english/Bugs/pkgreport-opts.inc:147
msgid "experimental"
msgstr "تجريبية"

#: ../../english/Bugs/pkgreport-opts.inc:148
msgid "unstable"
msgstr "غير مستقرة"

#: ../../english/Bugs/pkgreport-opts.inc:152
msgid "Unarchived"
msgstr "غير مؤرشفة"

#: ../../english/Bugs/pkgreport-opts.inc:155
msgid "Archived"
msgstr "مؤرشفة"

#: ../../english/Bugs/pkgreport-opts.inc:158
msgid "Archived and Unarchived"
msgstr "مؤرشفة وغير مؤرشفة"
