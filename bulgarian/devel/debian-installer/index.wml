#use wml::debian::template title="Debian-Installer" NOHEADER="true"
#use wml::debian::translation-check translation="a22870164df5007ae4f4e356dfe54983be0f1e9e"
#use wml::debian::recent_list
#include "$(ENGLISHDIR)/releases/info"
#include "$(ENGLISHDIR)/devel/debian-installer/images.data"

<h1>Новини</h1>

<p><:= get_recent_list('News/$(CUR_YEAR)', '2',
'$(ENGLISHDIR)/devel/debian-installer', '', '\d+\w*' ) :>
<a href="News">Стари новини</a>
</p>

<h1>Инсталиране на Дебиан с инсталатора</h1>


<p>
<if-stable-release release="bullseye">
<strong>За официалните носители и информация за инсталиране на Дебиан
<current_release_bullseye></strong> вижте
<a href="$(HOME)/releases/bullseye/debian-installer">страницата за bullseye</a>.
</if-stable-release>
<if-stable-release release="bookworm">
<strong>За официалните носители и информация за инсталиране на Дебиан
<current_release_bookworm></strong> вижте
<a href="$(HOME)/releases/bookworm/debian-installer">страницата за bookworm</a>.
</if-stable-release>
</p>

<div class="tip">
<p>
Всички образи по-долу са за версията на инсталатора, която се разработва за
следващото издание на Дебиан и по подразбиране ще инсталират тестовата версия
на Дебиан (<q><current_testing_name></q>).
</p>
</div>

<!-- Shown in the beginning of the release cycle: no Alpha/Beta/RC released yet. -->
<if-testing-installer released="no">
<p>

<strong>За инсталиране на тестовата версия на Дебиан</strong> се препоръчва използването на <strong>ежедневните компилации</strong> на инсталатора. На разположение са следните образи:

</p>

</if-testing-installer>

<!-- Shown later in the release cycle: Alpha/Beta/RC available, point at the latest one. -->
<if-testing-installer released="yes">
<p>

<strong>За инсталиране на тестова версия на Дебиан</strong> се препоръчва
използването на изданието на инсталатора <strong><humanversion /></strong>
след проверка на <a href="errata">списъка с известни проблеми</a>.
На разположение са следните образи за <humanversion />:

</p>

<h2>Официалнo изданиe</h2>

<div class="line">
<div class="item col50">
<strong>компактдискове за мрежова инсталация</strong>
<netinst-images />
</div>

<div class="item col50 lastcol">
<strong>компактдискове за мрежова инсталация (чрез <a href="$(HOME)/CD/jigdo-cd">jigdo</a>)</strong>
<netinst-images-jigdo />
</div>

</div>

<div class="line">
<div class="item col50">
<strong>компактдискове</strong>
<full-cd-images />
</div>

<div class="item col50 lastcol">
<strong>DVD</strong>
<full-dvd-images />
</div>

</div>


<div class="line">
<div class="item col50">
<strong>компактдискове (чрез <a href="$(HOME)/CD/jigdo-cd">jigdo</a>)</strong>
<full-cd-jigdo />
</div>

<div class="item col50 lastcol">
<strong>DVD (чрез <a href="$(HOME)/CD/jigdo-cd">jigdo</a>)</strong>
<full-dvd-jigdo />
</div>

</div>

<div class="line">
<div class="item col50">
<strong>Blu-ray (чрез <a href="$(HOME)/CD/jigdo-cd">jigdo</a>)</strong>
<full-bd-jigdo />
</div>

<div class="item col50 lastcol">
<strong>други носители (зареждане от мрежата, USB и други)</strong>
<other-images />
</div>
</div>

<p>
Възможно е и инсталирането на <b>снимка</b> на тестовата версия на
Дебиан.
Седмичните компилации създават пълен комплект образи, а дневните - ограничен набор.
</p>

<div class="warning">

<p>
Тези снимки инсталират тестовото издание на Дебиан, но самият инсталатор е базиран на пакети от нестабилното издание.
</p>

</div>


<h2>Седмични „снимки“ от клона за разработка</h2>

<div class="line">
<div class="item col50">
<strong>компактдискове</strong>
<devel-full-cd-images />
</div>

<div class="item col50 lastcol">
<strong>DVD</strong>
<devel-full-dvd-images />
</div>
</div>

<div class="line">
<div class="item col50">
<strong>компактдискове (чрез <a href="$(HOME)/CD/jigdo-cd">jigdo</a>)</strong>
<devel-full-cd-jigdo />
</div>

<div class="item col50 lastcol">
<strong>DVD (чрез <a href="$(HOME)/CD/jigdo-cd">jigdo</a>)</strong>
<devel-full-dvd-jigdo />
</div>
</div>

<div class="line">
<div class="item col50">
<strong>Blu-ray (чрез <a href="$(HOME)/CD/jigdo-cd">jigdo</a>)</strong>
<devel-full-bd-jigdo />
</div>
</div>

</if-testing-installer>

<h2>Ежедневни компилации от клона за разработка</h2>

<div class="line">
<div class="item col50">
<strong>компактдискове за инсталиране през мрежата</strong>
<devel-small-cd-images />
</div>

<div class="item col50 lastcol">
<strong>компактдисков за инсталиране през мрежата (чрез <a href="$(HOME)/CD/jigdo-cd">jigdo</a>)</strong>
<devel-small-cd-jigdo/>
</div>
</div>

<div class="line">
<div class="item col50">
<strong>други носители (зареждане от мрежата, USB и т.н.)</strong>
<devel-other-images />
</div>
</div>

<hr />

<p>
<strong>Бележки</strong>
</p>
<ul>
#        <li>Преди изтегляне на дневните снимки се препоръчва проверяването на
#            списъка с <a
#            href="https://wiki.debian.org/DebianInstaller/Today">известни
#            проблеми</a>.</li>
	<li>Някои архитектури може временно да са липсват от списъка с образи ако ежедневната компилация е срещнала проблем.</li>
	<li>Файловете с контролни суми за проверка на изтеглените образи (<tt>SHA512SUMS</tt> и <tt>SHA256SUMS</tt>) са в папката съдържаща образите.</li>
	<li>Препоръчваме jigdo за изтегляне на образи на
            големите компактдискове и DVD.</li>
	<li>Предлаганите за директно изтегляне образи на носители са
            ограничен набор, защото повечето потребители така или иначе нямат
            нужда от всичкия наличен софтуер. За пестене на място на сървърите
            и техните огледала, пълния комплект образи се предлага само само
            чрез jigdo.</li>
</ul>

<p>
<strong>След като използвате инсталатора</strong> ни изпратете
<a href="https://d-i.debian.org/manual/en.amd64/ch05s04.html#submit-bug">\
доклад за инсталацията</a>, дори и да не сте срещнали проблеми.
</p>

<h1>Документация</h1>

<p>
<strong>Ако възнамерявате да прочетете само един документ</strong> преди да
инсталирате, прочетете <a
href="https://d-i.debian.org/manual/en.amd64/apa.html">Инструкцията за
инсталиране</a> — бърз преглед на процеса на инсталиране. Други полезни
документи:
</p>

<ul>
<li>Ръководство за инсталиране:
#    <a href="$(HOME)/releases/stable/installmanual">за последното издание</a>
#    &mdash;
    <a href="$(HOME)/releases/testing/installmanual">за следващото издание (testing)</a>
    &mdash;
    <a href="https://d-i.debian.org/manual/">последна версия (в процес на разработка)</a>
<br />
подробно указание за инсталиране</li>
<li><a href="https://wiki.debian.org/DebianInstaller/FAQ">Отговори на често задавани въпроси за инсталатора</a>
и <a href="$(HOME)/CD/faq/">дисковете с Дебиан</a><br />
отговори на чести въпроси</li>
<li><a href="https://wiki.debian.org/DebianInstaller">Уики на инсталатора</a><br />
документация, поддържана от общността</li>
</ul>

<h1>Връзка с нас</h1>

<p>
Пощенският списък <a
href="https://lists.debian.org/debian-boot/">debian-boot</a> е основния форум за
дискусии и работа по инсталатора на Дебиан.
</p>

<p>
Имаме и канал в IRC — #debian-boot в <tt>irc.debian.org</tt>. Използва се
основно за разработка и отчасти за поддръжка. Ако не получите отговор на
въпроса си, опитайте да го зададете в пощенския списък.
</p>
