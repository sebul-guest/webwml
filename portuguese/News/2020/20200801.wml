#use wml::debian::translation-check translation="846effa858f46cf6429f61e241ec96292032972f"
<define-tag pagetitle>Atualização Debian 10: 10.5 lançado</define-tag>
<define-tag release_date>2020-08-01</define-tag>
#use wml::debian::news

<define-tag release>10</define-tag>
<define-tag codename>buster</define-tag>
<define-tag revision>10.5</define-tag>

<define-tag dsa>
    <tr><td align="center"><a href="$(HOME)/security/%0/dsa-%1">DSA-%1</a></td>
        <td align="center"><:
    my @p = ();
    for my $p (split (/,\s*/, "%2")) {
	push (@p, sprintf ('<a href="https://packages.debian.org/src:%s">%s</a>', $p, $p));
    }
    print join (", ", @p);
:></td></tr>
</define-tag>

<define-tag correction>
    <tr><td><a href="https://packages.debian.org/src:%0">%0</a></td>              <td>%1</td></tr>
</define-tag>

<define-tag srcpkg><a href="https://packages.debian.org/src:%0">%0</a></define-tag>

<p>O projeto Debian tem o prazer de anunciar a quinta atualização de sua
versão estável (stable) do Debian <release> (codename <q><codename></q>). 
Esta versão pontual adiciona principalmente correções para problemas de
segurança, junto com alguns ajustes para problemas sérios. Avisos de segurança
já foram publicados em separado e são referenciados quando disponíveis.</p>

<p>Essa versão pontual também aborda o Debian Security Advisory: 
<a href="https://www.debian.org/security/2020/dsa-4735">DSA-4735-1 grub2 -- security update</a> 
que cobre multiplas questões de CVE relativas a   
<a href="https://www.debian.org/security/2020-GRUB-UEFI-SecureBoot/">vulnerabilidade GRUB2 UEFI SecureBoot 'BootHole'</a>.</p>

<p>Por favor note que a versão pontual não constitui uma nova versão
do Debian <release>, mas apenas atualiza alguns dos pacotes inclusos. Não há
necessidade de se desfazer das antigas mídias do <q><codename></q>. Após a
instalação, os pacotes podem ser atualizados para as versões atuais usando um
espelho atualizado do Debian.</>

<p>As essoas que frequentemente instalam atualizações do security.debian.org
não terão que atualizar muitos pacotes, e a maioria dessas atualizações estão
incluídas na versão pontual.</p>

<p>Novas imagens de instalação logo estarão disponíveis nos locais
habituais.</p>

<p>A atualização de uma instalação existente para esta revisão pode ser feita 
apontando o sistema de gerenciamento de pacotes para um dos muitos espelhos 
HTTP do Debian. Uma lista abrangente de espelhos está disponível em:</p>

<div class="center">
  <a href="$(HOME)/mirror/list">https://www.debian.org/mirror/list</a>
</div>




<h2>Correções de bugs gerais</h2>

<p>Esta atualização estável (stable) adiciona algumas correções importantes 
para os seguintes pacotes:</p>

<table border=0>
<tr><th>Pacote</th>               <th>Justificativa</th></tr>
<correction appstream-glib "Corrige falhas na construção em 2020 e posteriores">
<correction asunder "Usa gnudb por padrão em vez de freedb">
<correction b43-fwcutter "Assegura a remoção com sucesso sob locales não
ingleses; não falha a remoção se alguns arquivos não existirem; Corrige
dependências ausentes em pciutils e ca-certificates">
<correction balsa "Fornece identidade ao servidor ao validar certificados,
permitindo uma validação bem sucedida ao utilizar correção (patch)
glib-networking para CVE-2020-13645">
<correction base-files "Atualização para a versão pontual">
<correction batik "Corrige falsificação de requisição server-side através de
atributos xlink:href [CVE-2019-17566]">
<correction borgbackup "Corrige bug de corrupção de índice que leva à perda de dados">
<correction bundler "Atualiza a versão requerida de ruby-molinillo">
<correction c-icap-modules "Adiciona suporte para ClamAV 0.102">
<correction cacti "Corrige problema com as marcações de dia e hora UNIX depois
de 13 de setembro de 2020, as quais são rejeitadas como início/fim de gráfico;
Corrige execução de código remoto [CVE-2020-7237], cross-site scripting
[CVE-2020-7106], problema CSRF [CVE-2020-13231]; desabilitar uma conta de
usuário(a) não invalida imediatamente as permissões [CVE-2020-13230]">
<correction calamares-settings-debian "Habilita o módulo displaymanager,
corrigindo as opções de autologins; usa xdg-user-dir para especificar diretório
de área de trabalho">
<correction clamav "Nova versão original (upstream); correções de segurança
[CVE-2020-3327 CVE-2020-3341 CVE-2020-3350 CVE-2020-3327 CVE-2020-3481]">
<correction cloud-init "Nova versão original (upstream">
<correction commons-configuration2 "Previne criação de objetos quando carregar
arquivos YAML [CVE-2020-1953]">
<correction confget "Corrige manipulação Python de valores contendo <q>=</q>">
<correction dbus "Nova versão original estável (upstream); previne problema de
negação de serviço [CVE-2020-12049]; previne uso-após-livre se dois nopes de
usuários compartilharem uma uid">
<correction debian-edu-config "Corrige perda de endereço IPv4 alocado
dinamicamente">
<correction debian-installer "Atualizado kernel Linux ABI para 4.19.0-10">
<correction debian-installer-netboot-images "Reconstruído contra as atualizações
propostas">
<correction debian-ports-archive-keyring "Aumenta a data de validade da chave de
2020 (84C573CD4E1AFD6C) por um ano; adiciona a chave Debian Ports Archive
Automatic Signing (2021); move a chave de 2018 (ID: 06AED62430CB581C) para o
chaveiro de chaves removidas">
<correction debian-security-support "Atualiza o status de suporte de diversos
pacotes">
<correction dpdk "Nova versão upstream">
<correction exiv2 "Ajusta patch de segurança excessivamente restritivo
[CVE-2018-10958 e CVE-2018-10999]; corrige problema de negação de serviço
[CVE-2018-16336]">
<correction fdroidserver "Corrige endereço de validação Litecoin">
<correction file-roller "Correção de segurança [CVE-2020-11736]">
<correction freerdp2 "Corrige logins de smartcard; correções de segurança
[CVE-2020-11521 CVE-2020-11522 CVE-2020-11523 CVE-2020-11524 CVE-2020-11525
CVE-2020-11526]">
<correction fwupd "Nova versão upstream; corrige possível problema de
verificação de assinatura [CVE-2020-10759]; uso de chaves de assinatura
rotacionadas do Debian">
<correction fwupd-arm64-signed "Nova versão upstream; corrige possível problema
de verificação de assinatura [CVE-2020-10759]; uso de chaves de assinatura
rotacionadas do Debian">
<correction fwupd-armhf-signed "Nova versão upstream; corrige possível problema
de verificação de assinatura [CVE-2020-10759]; uso de chaves de assinatura
rotacionadas do Debian">
<correction fwupd-i386-signed "Nova versão upstream; corrige possível problema
de verificação de assinatura [CVE-2020-10759]; uso de chaves de assinatura
rotacionadas do Debian">
<correction fwupdate "Uso de chave de assinatura rotacionada do Debian">
<correction fwupdate-amd64-signed "Uso de chave de assinatura rotacionada do
Debian">
<correction fwupdate-arm64-signed "Uso de chave de assinatura rotacionada do
Debian">
<correction fwupdate-armhf-signed "Uso de chave de assinatura rotacionada do
Debian">
<correction fwupdate-i386-signed "Uso de chave de assinatura rotacionada do
Debian">
<correction gist "Evita API de autorização obsoleta">
<correction glib-networking "Retorna erro de identificação ruim caso identidade
não esteja configurada [CVE-2020-13645]; quebra balsa anteriores a
2.5.6-2+deb10u1 por conta da correção CVE-2020-13645 quebrar a verificação de
certificado de balsa">
<correction gnutls28 "Corrige erros de presunções em TL1.2; Corrige vazamento de
memória; trata tíquetes de sessão de comprimento zero, corrige erros de conexão
em sessão TLS1.2 para grandes provedores de hospedagem; corrige erro de
verificação com cadeias alternadas">
<correction intel-microcode "Retorna alguns microcódigos para versões
anteriores, contornando suspensões no boot em Skylake-U/Y e Skylake Xeon E3">
<correction jackson-databind "Corrige múltiplos problemas de segurança afetando
BeanDeserializerFactory [CVE-2020-9548 CVE-2020-9547 CVE-2020-9546 CVE-2020-8840
CVE-2020-14195 CVE-2020-14062 CVE-2020-14061 CVE-2020-14060 CVE-2020-11620
CVE-2020-11619 CVE-2020-11113 CVE-2020-11112 CVE-2020-11111 CVE-2020-10969
CVE-2020-10968 CVE-2020-10673 CVE-2020-10672 CVE-2019-20330 CVE-2019-17531 e
CVE-2019-17267]">
<correction jameica "Adiciona mckoisqldb ao classpath, permitindo uso do plugin
SynTAX">
<correction jigdo "Corrige suporte HTTPS em jigdo-lite e jigdo-mirror">
<correction ksh "Corrige problema de restrição de variáveis ambientais
[CVE-2019-14868]">
<correction lemonldap-ng "Corrige regressão de configuração no nginx introduzida
pela correção para a CVE-2019-19791">
<correction libapache-mod-jk "Renomeia o arquivo de configuração do Apache para
ser automaticamente habilitado e desabilitado">
<correction libclamunrar "Nova versão estável upstream; adiciona metapacote não
versionado">
<correction libembperl-perl "Trata páginas de erro do Apache &gt;= 2.4.40">
<correction libexif "Correções de segurança [CVE-2020-12767 CVE-2020-0093
CVE-2020-13112 CVE-2020-13113 CVE-2020-13114]; corrige estouro de buffer
[CVE-2020-0182] e estouro de inteiros [CVE-2020-0198]">
<correction libinput "Quirks: adiciona atributo de integração de trackpoint">
<correction libntlm "Corrige estouro de buffer [CVE-2019-17455]">
<correction libpam-radius-auth "Corrige estouro de buffer no campo de senha
[CVE-2015-9542]">
<correction libunwind "Corrige falhas de segmentação (segfaults) na arquitetura
mips; ativa manualmente o suporte a exceções C++ somente em i386 e amd64">
<correction libyang "Corrige quebra de corrupção de cache, CVE-2019-19333,
CVE-2019-19334">
<correction linux "Nova versão estável upstream">
<correction linux-latest "Atualização para o Kernel Linux ABI 4.19.0-10">
<correction linux-signed-amd64 "Nova versão upstream estável">
<correction linux-signed-arm64 "Nova versão upstream estável">
<correction linux-signed-i386 "Nova versão upstream estável">
<correction lirc "Corrige gerenciamento de conffile">
<correction mailutils "maidag: desativa privilégio setuid para todos as
operações de entrega, exceto mda [CVE-2019-18862]">
<correction mariadb-10.3 "Nova versão upstream estável; correções de segurança
[CVE-2020-2752 CVE-2020-2760 CVE-2020-2812 CVE-2020-2814 CVE-2020-13249];
corrige detecção de regressão em RocksDB ZSTD">
<correction mod-gnutls "Corrige possível falha de segmentação (segfault) em
falha de TLS handshake; corrige falhas de testes">
<correction multipath-tools "kpartx: Uso de caminho correto na regra udev para partx">
<correction mutt "Não checa criptografia IMAP PREAUTH se $tunnel estiver em uso">
<correction mydumper "Ligação com libm">
<correction nfs-utils "statd: pega a user-id a partir de /var/lib/nfs/sm
[CVE-2019-3689]; não torna /var/lib/nfs pertencente a statd">
<correction nginx "Corrige vulnerabilidade de contrabando de requisições de
página de erro [CVE-2019-20372]">
<correction nmap "Atualiza chave padrão para o tamanho de 2048 bits">
<correction node-dot-prop "Corrige regressão introduzida pelo reparo da
CVE-2020-8116">
<correction node-handlebars "Desautoriza chamada <q>helperMissing</q> e
<q>blockHelperMissing</q> diretamente [CVE-2019-19919]">
<correction node-minimist "Corrige poluição de protótipo [CVE-2020-7598]">
<correction nvidia-graphics-drivers "Nova versão upstream estável; correções de
segurança [CVE-2020-5963 CVE-2020-5967]">
<correction nvidia-graphics-drivers-legacy-390xx "Nova versão upstream estável;
correções de segurança [CVE-2020-5963 CVE-2020-5967]">
<correction openstack-debian-images "Instala resolvconf se estiver instalando
cloud-init">
<correction pagekite "Evita problemas com validade expirada de certificados SSL
enviados utilizando aqueles certificados do pacote ca-certificates">
<correction pdfchain "Corrige falha na inicialização">
<correction perl "Corrige múltiplos problemas de segurança relacionados a
expressões regulares [CVE-2020-10543 CVE-2020-10878 CVE-2020-12723]">
<correction php-horde "Corrige vulnerabilidade de cross-site scripting [CVE-2020-8035]">
<correction php-horde-gollem "Corrige vulnerabilidade de cross-site scripting na
saída breadcrumb [CVE-2020-8034]">
<correction pillow "Corrige múltiplos problemas de leitura fora dos limites
[CVE-2020-11538 CVE-2020-10378 CVE-2020-10177]">
<correction policyd-rate-limit "Corrige problema de responsabilidade devido a
reuso de socket">
<correction postfix "Nova versão upstream estável; corrige falha de segmentação
(segfault) no papel de cliente tlsproxy quanto o papel de servidor foi
desabilitado; corrige <q> valor padrão de maillog_file_rotate_suffix default
value utilizou minuto no lugar de mês</q>; corrige diversos problemas
relacionados a TLS; correções no README.Debian">
<correction python-markdown2 "Corrige problema de cross-site scripting
[CVE-2020-11888]">
<correction python3.7 "Evita loop infinito ao ler arquivos tar especialmente
montados usando o módulo tarfile [CVE-2019-20907]; resolve colisão de hash para
IPv4Interface e IPv6Interface [CVE-2020-14422]; corrige problema de negação de
serviço em urllib.request.AbstractBasicAuthHandler [CVE-2020-8492]">
<correction qdirstat "Corrige salvamento de categorias MIME configuradas pelo(a)
usuário(a)">
<correction raspi3-firmware "Corrige erro de digitação que poderia levar a
sistema não inicializável">
<correction resource-agents "IPsrcaddr: Torna <q>proto</q> opcional para
corrigir regressão quando utilizado sem NetworkManager">
<correction ruby-json "Corrige vulnerabilidade de criação de objetos inseguros
[CVE-2020-10663]">
<correction shim "Utiliza chaves de assinatura Debian rotacionadas">
<correction shim-helpers-amd64-signed "Utiliza chaves de assinatura Debian
rotacionadas">
<correction shim-helpers-arm64-signed "Utiliza chaves de assinatura Debian
rotacionadas">
<correction shim-helpers-i386-signed "Utiliza chaves de assinatura Debian
rotacionadas">
<correction speedtest-cli "Envia os cabeçalhos corretos para corrigir teste de
velocidade de upload ">
<correction ssvnc "Corrige escrita fora dos limites [CVE-2018-20020], laço
infinito [CVE-2018-20021], inicialização imprópria [CVE-2018-20022], negação de
serviço em potencial [CVE-2018-20024]">
<correction storebackup "Corrige possível vulnerabilidade de escalação de
privilégios [CVE-2020-7040]">
<correction suricata "Corrige privilégios de queda em modo nflog runmode">
<correction tigervnc "Não utiliza libunwind em armel, armhf ou arm64">
<correction transmission "Corrige possível problema de negação de serviço
[CVE-2018-10756]">
<correction wav2cdr "Usa tipos inteiros de tamanho fixo C99 para corrigir a
indicação de tempo de execução em arquiteturas de 64bits diferentes amd64 e
alpha">
<correction zipios++ "Correções de segurança [CVE-2019-13453]">
</table>


<h2>Atualizações de segurança</h2>


<p>Essa revisão adiciona as seguintes atualizações de segurança para a versão
estável (stable).
A equipe de segurança já lançou um aviso para cada uma dessas atualizações:</p>

<table border=0>
<tr><th>ID do aviso</th>  <th>Pacote</th></tr>
<dsa 2020 4626 php7.3>
<dsa 2020 4674 roundcube>
<dsa 2020 4675 graphicsmagick>
<dsa 2020 4676 salt>
<dsa 2020 4677 wordpress>
<dsa 2020 4678 firefox-esr>
<dsa 2020 4679 keystone>
<dsa 2020 4680 tomcat9>
<dsa 2020 4681 webkit2gtk>
<dsa 2020 4682 squid>
<dsa 2020 4683 thunderbird>
<dsa 2020 4684 libreswan>
<dsa 2020 4685 apt>
<dsa 2020 4686 apache-log4j1.2>
<dsa 2020 4687 exim4>
<dsa 2020 4688 dpdk>
<dsa 2020 4689 bind9>
<dsa 2020 4690 dovecot>
<dsa 2020 4691 pdns-recursor>
<dsa 2020 4692 netqmail>
<dsa 2020 4694 unbound>
<dsa 2020 4695 firefox-esr>
<dsa 2020 4696 nodejs>
<dsa 2020 4697 gnutls28>
<dsa 2020 4699 linux-signed-amd64>
<dsa 2020 4699 linux-signed-arm64>
<dsa 2020 4699 linux-signed-i386>
<dsa 2020 4699 linux>
<dsa 2020 4700 roundcube>
<dsa 2020 4701 intel-microcode>
<dsa 2020 4702 thunderbird>
<dsa 2020 4704 vlc>
<dsa 2020 4705 python-django>
<dsa 2020 4707 mutt>
<dsa 2020 4708 neomutt>
<dsa 2020 4709 wordpress>
<dsa 2020 4710 trafficserver>
<dsa 2020 4711 coturn>
<dsa 2020 4712 imagemagick>
<dsa 2020 4713 firefox-esr>
<dsa 2020 4714 chromium>
<dsa 2020 4716 docker.io>
<dsa 2020 4718 thunderbird>
<dsa 2020 4719 php7.3>
<dsa 2020 4720 roundcube>
<dsa 2020 4721 ruby2.5>
<dsa 2020 4722 ffmpeg>
<dsa 2020 4723 xen>
<dsa 2020 4724 webkit2gtk>
<dsa 2020 4725 evolution-data-server>
<dsa 2020 4726 nss>
<dsa 2020 4727 tomcat9>
<dsa 2020 4728 qemu>
<dsa 2020 4729 libopenmpt>
<dsa 2020 4730 ruby-sanitize>
<dsa 2020 4731 redis>
<dsa 2020 4732 squid>
<dsa 2020 4733 qemu>
<dsa 2020 4735 grub-efi-amd64-signed>
<dsa 2020 4735 grub-efi-arm64-signed>
<dsa 2020 4735 grub-efi-ia32-signed>
<dsa 2020 4735 grub2>
</table>


<h2>Pacotes removidos</h2>

<p>Os seguintes pacotes foram removidos devido a circunstâncias além do nosso
controle:</p>

<table border=0>
<tr><th>Pacote</th>               <th>Justificativa</th></tr>
<correction golang-github-unknwon-cae "Problemas de segurança; sem manutenção">
<correction janus "Não suportado pela versão estável (stable)">
<correction mathematica-fonts "Depende de local de download indisponível">
<correction matrix-synapse "Problemas de segurança; não suportado">
<correction selenium-firefoxdriver "Incompatível com versões mais atuais de Firefox ESR">

</table>

<h2>Debian Installer</h2>
<p>O instalador foi atualizado para incluir as correções incorporadas na versão
estável (stable) pela versão pontual.</p>

<h2>URLs</h2>

<p>As listas completas dos pacotes que foram alterados por esta revisão:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/<downcase <codename>>/ChangeLog">
</div>

<p>A atual versão estável (stable):</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/stable/">
</div>

<p>Atualizações propostas (proposed updates) para a versão estável (stable)::</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/proposed-updates">
</div>

<p>Informações da versão estável (stable) (notas de lançamento, errata, etc):</p>

<div class="center">
  <a
  href="$(HOME)/releases/stable/">https://www.debian.org/releases/stable/</a>
</div>

<p>Anúncios de segurança e informações:</p>

<div class="center">
  <a href="$(HOME)/security/">https://www.debian.org/security/</a>
</div>

<h2>Sobre o  Debian</h2>

<p>O projeto Debian é uma associação de desenvolvedores(as) de software livre 
que dedicam seu tempo e esforço como voluntários(as) para produzir o sistema 
operacional completamente livre Debian.</p>

<h2>Informações de contato</h2>

<p>Para mais informações, por favor visite as páginas web do Debian em
<a href="$(HOME)/">https://www.debian.org/</a>, envie um e-mail (em inglês) para
&lt;press@debian.org&gt;, ou entre em contato (em inglês) com o time de
lançamento da estável (stable) em &lt;debian-release@lists.debian.org&gt;.</p>
