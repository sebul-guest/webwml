#use wml::debian::ddp title="Handleidingen voor Ontwikkelaars van Debian"
#include "$(ENGLISHDIR)/doc/manuals.defs"
#include "$(ENGLISHDIR)/doc/devel-manuals.defs"
#use wml::debian::translation-check translation="fca004f92b97a053bc89c121827943f1eae0531b"

# Last Translation Update by $Author$
# Last Translation Update at $Date$

<document "Debian Policy Manual" "policy">

<div class="centerblock">
<p>
  Deze handleiding beschrijft de beleidsvereisten voor de Debian
  GNU/Linux-distributie.
  Dit omvat ook de structuur en inhoud van het Debian-archief, verschillende
  keuzes in verband met het ontwerp van het besturingssysteem, alsook
  technische vereisten waaraan elk pakket moet voldoen om te worden
  opgenomen in de distributie.

<doctable>
  <authors "Ian Jackson, Christian Schwarz, David A. Morris">
  <maintainer "De Debian Policy-groep">
  <status>
  afgewerkt
  </status>
  <availability>
  <inpackage "debian-policy">
  <inddpvcs-debian-policy>
  <p>
  <a href="https://bugs.debian.org/debian-policy">Voorgestelde amendmenten</a>
op Debian Policy
</p>

  <p>Aanvullende Policy-documentatie:</p>
  <ul>
    <li><a href="packaging-manuals/fhs/fhs-3.0.html">Standaard voor de Bestandssysteemhiërarchie (FHS)</a>
    [<a href="packaging-manuals/fhs/fhs-3.0.pdf">PDF</a>]
    [<a href="packaging-manuals/fhs/fhs-3.0.txt">platte tekst</a>]
    <li><a href="debian-policy/upgrading-checklist.html">Checklist voor opwaardering</a>
    <li><a href="packaging-manuals/virtual-package-names-list.yaml">Lijst van namen van virtuele pakketten</a>
    <li><a href="packaging-manuals/menu-policy/">Menu-beleid</a>
    [<a href="packaging-manuals/menu-policy/menu-policy.txt.gz">platte tekst</a>]
    <li><a href="packaging-manuals/perl-policy/">Perl-beleid</a>
    [<a href="packaging-manuals/perl-policy/perl-policy.txt.gz">platte tekst</a>]
    <li><a href="packaging-manuals/debconf_specification.html">debconf specificatie</a>
    <li><a href="packaging-manuals/debian-emacs-policy">Emacsen-beleid</a>
    <li><a href="packaging-manuals/java-policy/">Java-beleid</a>
    <li><a href="packaging-manuals/python-policy/">Python-beleid</a>
    <li><a href="packaging-manuals/copyright-format/1.0/">copyright-indeling specificatie</a>
  </ul>
  </availability>
</doctable>
</div>

<hr>

<document "Debian Developer's Reference" "devref">

<div class="centerblock">
<p>
  Deze handleiding beschrijft procedures en bronnen voor Debian-beheerders.
  Het beschrijft hoe u een nieuwe ontwikkelaar wordt, de upload-procedure, hoe
  werken met het Bug Tracking Systeem (BTS), de mailinglijsten,
  Internet-servers, enz.

  <p>Deze handleiding is bedoeld als een <em>referentie-handleiding</em> voor
  alle Debian-ontwikkelaars (beginners en oude rotten).

<doctable>
  <authors "Ian Jackson, Christian Schwarz, Lucas Nussbaum, Rapha&euml;l Hertzog, Adam Di Carlo, Andreas Barth">
  <maintainer "Lucas Nussbaum, Hideki Yamane, Holger Levsen">
  <status>
  afgewerkt
  </status>
  <availability>
  <inpackage "developers-reference">
  <inddpvcs-developers-reference>
  </availability>
</doctable>
</div>

<hr>

<document "Guide for Debian Maintainers" "debmake-doc">

<div class="centerblock">
<p>
Deze tutorial beschrijft voor gebruikers en toekomstige ontwikkelaars
hoe een Debian pakket gebouwd wordt met het commando <code>debmake</code>.
</p>
<p>
Hij focust op de moderne manier van pakketten bouwen en
bevat vele eenvoudige voorbeelden.
</p>
<ul>
<li>POSIX shell script werkwijze voor het bouwen van pakketten</li>
<li>Python3 script werkwijze voor het bouwen van pakketten</li>
<li>C met Makefile/Autotools/CMake</li>
<li>verschillende binaire pakketten met gedeelde bibliotheek etc.</li>
</ul>
<p>
Deze “Guide for Debian Maintainers” mag beschouwd worden als
de opvolger van “Debian New Maintainers’ Guide”.
</p>

<doctable>
  <authors "Osamu Aoki">
  <maintainer "Osamu Aoki">
  <status>
  afgewerkt
  </status>
  <availability>
  <inpackage "debmake-doc">
  <inddpvcs-debmake-doc>
  </availability>
</doctable>
</div>

<hr>

<document "Debian New Maintainers' Guide" "maint-guide">

<div class="centerblock">
<p>
  Dit document tracht te beschrijven hoe u een Debian GNU/Linux-pakket
  maakt als gewone Debian-gebruiker (en wannabe-ontwikkelaar) in gewone taal en
  ruim voorzien van werkende voorbeelden.

  <p>In tegenstelling tot vorige pogingen, is deze gebaseerd op <code>debhelper</code>
  en de nieuwe hulpmiddelen die ter beschikking staan van beheerders. De auteur tracht
  met alle middelen om vorige inspanningen te incorporeren en op dezelfde lijn
  te krijgen.

<doctable>
  <authors "Josip Rodin, Osamu Aoki">
  <maintainer "Osamu Aoki">
  <status>
  verouderd, gebruik bovenstaande “Guide for Debian Maintainers” (debmake-doc)
  </status>
  <availability>
  <inpackage "maint-guide">
  <inddpvcs-maint-guide>
  </availability>
</doctable>
</div>

<hr>

<document "Introduction to Debian packaging" "packaging-tutorial">

<div class="centerblock">
<p>
Deze tutorial biedt een inleiding in het maken van pakketten voor Debian.
Het leert toekomstige ontwikkelaars hoe ze bestaande pakketten kunnen
aanpassen, hoe ze eigen pakketten kunnen maken en hoe te communiceren met de
Debian-gemeenschap.
Naast de algemene tutorial zijn er ook drie oefensessies over het aanpassen van
het <code>grep</code>-pakket en het maken van een pakket voor het spel
<code>gnujump</code> en een Javabibliotheek.
</p>

<doctable>
  <authors "Lucas Nussbaum">
  <maintainer "Lucas Nussbaum">
  <status>
  afgewerkt
  </status>
  <availability>
  <inpackage "packaging-tutorial">
  <inddpvcs-packaging-tutorial>
  </availability>
</doctable>
</div>

<hr>

<document "Debian Menu System" "menu">

<div class="centerblock">
<p>
  Deze handleiding beschrijft het Debian Menu Systeem en het <strong>menu</strong>-pakket.

  <p>Het menu-pakket is geïnspireerd op het install-fvwm2-menu programma uit het
  oude fvwm2-pakket. Echter, menu tracht een meer algemene interface aan te
  bieden voor het maken van menu's. Met het update-menus commando van dit
  pakket, moeten er geen pakketten opnieuw gewijzigd worden voor elke
  X-vensterbeheerder en het biedt eenzelfde interface aan voor zowel tekst-
  als X-georiënteerde programma's.

<doctable>
  <authors "Joost Witteveen, Joey Hess, Christian Schwarz">
  <maintainer "Joost Witteveen">
  <status>
  afgewerkt
  </status>
  <availability>
  <inpackage "menu">
  <a href="packaging-manuals/menu.html/">HTML online</a>
  </availability>
</doctable>
</div>

<hr>

<document "Debian Installer internals" "d-i-internals">

<div class="centerblock">
<p>
  Dit document is bedoeld om het installatiesysteem van Debian meer
  toegankelijk te maken voor nieuwe ontwikkelaars. Het fungeert ook als
  de centrale bron van informatie over de technische aspecten ervan.

<doctable>
  <authors "Frans Pop">
  <maintainer "Debian Installer team">
  <status>
  ready
  </status>
  <availability>
  <p><a href="https://d-i.debian.org/doc/internals/">HTML online</a>.</p>
  <p><a href="https://salsa.debian.org/installer-team/debian-installer/tree/master/doc/devel/internals">DocBook XML broncode online</a></p>
  </availability>
</doctable>
</div>

<hr>

<document "dbconfig-common documentation" "dbconfig-common">

<div class="centerblock">
<p>
  Dit document is bedoeld voor pakketbeheerders die pakketten onderhouden
  die een werkende database nodig hebben. In plaats van zelf de vereiste
  logische stappen toe te passen, kunnen ze beroep doen op dbconfig-common
  om voor hen de juiste vragen te stellen tijdens het installeren,
  opwaarderen, herconfigureren en de-installeren en om de database
  aan te maken en op te vullen.

<doctable>
  <authors "Sean Finney and Paul Gevers">
  <maintainer "Paul Gevers">
  <status>
  ready
  </status>
  <availability>
  <inpackage "dbconfig-common">
  <inddpvcs-dbconfig-common>
  Daarnaast is ook het <a href="/doc/manuals/dbconfig-common/dbconfig-common-design.html">designdocument</a> beschikbaar.
  </availability>
</doctable>
</div>

<hr>

<document "dbapp-policy" "dbapp-policy">

<div class="centerblock">
<p>
  Een voorstel van beleidsrichtlijnen voor pakketten
  die van een werkende database afhangen.

<doctable>
  <authors "Sean Finney">
  <maintainer "Paul Gevers">
  <status>
  draft
  </status>
  <availability>
  <inpackage "dbconfig-common">
  <inddpvcs-dbapp-policy>
  </availability>
</doctable>
</div>




