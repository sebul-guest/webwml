#use wml::debian::translation-check translation="b827d01d6caf2b68e865bd2c4ff78fab56ab8eb4"
<define-tag pagetitle>Debian 12 is bijgewerkt: 12.2 werd uitgebracht</define-tag>
<define-tag release_date>2023-10-07</define-tag>
#use wml::debian::news

<define-tag release>12</define-tag>
<define-tag codename>bookworm</define-tag>
<define-tag revision>12.2</define-tag>

<define-tag dsa>
    <tr><td align="center"><a href="$(HOME)/security/%0/dsa-%1">DSA-%1</a></td>
        <td align="center"><:
    my @p = ();
    for my $p (split (/,\s*/, "%2")) {
	push (@p, sprintf ('<a href="https://packages.debian.org/src:%s">%s</a>', $p, $p));
    }
    print join (", ", @p);
:></td></tr>
</define-tag>

<define-tag correction>
    <tr><td><a href="https://packages.debian.org/src:%0">%0</a></td>              <td>%1</td></tr>
</define-tag>

<define-tag srcpkg><a href="https://packages.debian.org/src:%0">%0</a></define-tag>

<p>Het Debian-project kondigt met genoegen de tweede update aan van zijn
stabiele distributie Debian <release> (codenaam <q><codename></q>).
Deze tussenrelease voegt voornamelijk correcties voor beveiligingsproblemen toe,
samen met een paar aanpassingen voor ernstige problemen. Beveiligingsadviezen
werden reeds afzonderlijk gepubliceerd en, waar beschikbaar, wordt hiernaar
verwezen.</p>

<p>Merk op dat de tussenrelease geen nieuwe versie van Debian <release> is,
maar slechts een update van enkele van de meegeleverde pakketten. Het is niet
nodig om oude media met <q><codename></q> weg te gooien. Na de installatie
kunnen pakketten worden opgewaardeerd naar de huidige versie door een
bijgewerkte Debian-spiegelserver te gebruiken.</p>

<p>Wie regelmatig updates installeert vanuit security.debian.org zal niet veel
pakketten moeten updaten, en de meeste van dergelijke updates zijn opgenomen in
de tussenrelease.</p>

<p>Nieuwe installatie-images zullen binnenkort beschikbaar zijn op de gewone
plaatsen.</p>

<p>Het upgraden van een bestaande installatie naar deze revisie kan worden
bereikt door het pakketbeheersysteem naar een van de vele HTTP-spiegelservers
van Debian te verwijzen. Een uitgebreide lijst met spiegelservers is
beschikbaar op:</p>

<div class="center">
  <a href="$(HOME)/mirror/list">https://www.debian.org/mirror/list</a>
</div>




<h2>Oplossingen voor diverse problemen</h2>

<p>Met deze update van stable, de stabiele distributie, worden een paar
belangrijke correcties aangebracht aan de volgende pakketten:</p>

<table border=0>
<tr><th>Pakket</th>               <th>Reden</th></tr>
<correction amd64-microcode "Bijwerken van meegeleverde microcode, inclusief een oplossing voor <q>AMD Inception</q> op AMD Zen4-processors [CVE-2023-20569]">
<correction arctica-greeter "Ondersteuning voor het configureren van het schermtoetsenbordthema via de gsettings van ArcticaGreeter; OSK lay-out <q>Compact</q> gebruiken (in plaats van Small) die speciale toetsen zoals Duitse Umlauts bevat; weergave van berichten over mislukte verificatie verbeteren; het thema active gebruiken in plaats van emerald">
<correction autofs "Regressie repareren die de bereikbaarheid op dual-stack-hosts bepaalt">
<correction base-files "Update voor tussenrelease 12.2">
<correction batik "Problemen met vervalsing van Server Side-verzoeken oplossen [CVE-2022-44729 CVE-2022-44730]">
<correction boxer-data "Niet langer https-everywhere installeren voor Firefox">
<correction brltty "xbrlapi: brltty niet proberen te starten met ba+a2 als het niet beschikbaar is; cursorrouting en braille-verplaatsing in Orca repareren wanneer xbrlapi is geïnstalleerd, maar het a2-schermstuurprogramma niet">
<correction ca-certificates-java "Tijdelijke oplossing voor niet-geconfigureerde JRE tijdens nieuwe installaties">
<correction cairosvg "Gegevens verwerken: URL's in veilige modus">
<correction calibre "Exportfunctie repareren">
<correction clamav "Nieuwe bovenstroomse stabiele release; beveiligingsoplossingen [CVE-2023-20197 CVE-2023-20212]">
<correction cryptmount "Problemen met geheugeninitialisatie vermijden in opdrachtregelverwerker">
<correction cups "Probleem met op heap gebaseerde bufferoverloop oplossen [CVE-2023-4504]; probleem met niet-geauthenticeerde toegang oplossen [CVE-2023-32360]">
<correction curl "Bouwen met OpenLDAP om onjuist ophalen van binaire LDAP-attributen te corrigeren; probleem met overmatig geheugengebruik oplossen [CVE-2023-38039]">
<correction cyrus-imapd "Ervoor zorgen dat postbussen niet verloren gaan bij upgrades van bullseye">
<correction dar "Problemen oplossen met het maken van geïsoleerde catalogi wanneer dar is gebouwd met een recente gcc-versie">
<correction dbus "Nieuwe bovenstroomse stabiele release; reparatie voor een dbus-daemon-crash tijdens het herladen van het beleid als een verbinding behoort tot een gebruikersaccount dat is verwijderd, of als een Name Service Switch plugin defect is, bij kernels die SO_PEERGROUPS niet ondersteunen; de fout correct rapporteren als het verkrijgen van de groepen van een uid mislukt; dbus-user-session: XDG_CURRENT_DESKTOP kopiëren naar activeringsomgeving">
<correction debian-archive-keyring "Overgebleven sleutelbossen opruimen in trusted.gpg.d">
<correction debian-edu-doc "Bijwerken van de handleiding voor Debian Edu Bookworm">
<correction debian-edu-install "Nieuwe bovenstroomse release; de automatische partitiegroottes van D-I aanpassen">
<correction debian-installer "Linux kernel ABI verhogen naar 6.1.0-13; opnieuw bouwen tegen proposed-updates">
<correction debian-installer-netboot-images "Opnieuw bouwen tegen proposed-updates">
<correction debian-parl "Opnieuw bouwen met nieuwere boxer-data; niet langer webext-https-everywhere vereisen">
<correction debianutils "Dubbele vermeldingen in /etc/shells repareren; /bin/sh beheren in het statusbestand; conform maken van shells in locaties onder alias oplossen">
<correction dgit "De oude map /updates voor beveiligingen enkel gebruiken voor buster; het pushen naar oudere versies die al in het archief zitten, voorkomen">
<correction dhcpcd5 "Opwaarderingen met overblijfsels van wheezy makkelijker maken; verouderde ntpd-integratie laten vallen; versie repareren in opruimscript">
<correction dpdk "Nieuwe bovenstroomse stabiele release">
<correction dput-ng "Toegestane uploaddoelen bijwerken; probleem met bouwen vanaf broncode oplossen">
<correction efibootguard "Oplossing voor onvoldoende of ontbrekende validatie en opschoning van invoer uit onbetrouwbare bootloader-omgevingsbestanden [CVE-2023-39950]">
<correction electrum "Een beveiligingsprobleem met Lightning oplossen">
<correction filezilla "Bouwen voor 32-bits architecturen repareren; oplossing voor crash bij het verwijderen van bestandstypes uit de lijst">
<correction firewalld "Geen IPv4- en IPv6-adressen vermengen in één enkele nftables-regel">
<correction flann "Extra -llz4 uit flann.pc verwijderen">
<correction foot "XTGETTCAP-opzoekingen met ongeldige hexadecimale coderingen negeren">
<correction freedombox "In apt-voorkeuren n= gebruiken voor vlotte upgrades">
<correction freeradius "Ervoor zorgen dat TLS-Client-Cert-Common-Name de juiste gegevens bevat">
<correction ghostscript "Probleem met bufferoverloop oplossen [CVE-2023-38559]; proberen het opstarten van de IJS-server te beveiligen [CVE-2023-43115]">
<correction gitit "Opnieuw bouwen tegen nieuw pandoc">
<correction gjs "Oneindige lussen van inactieve callbacks voorkomen als een inactieve verwerker wordt aangeroepen tijdens GC">
<correction glibc "De waarde van F_GETLK/F_SETLK/F_SETLKW met __USE_FILE_OFFSET64 op ppc64el repareren; oplossing voor een overloop bij het lezen van de stack in getaddrinfo in de modus no-aaaa [CVE-2023-4527]; oplossing voor gebruik  na vrijgave in getcanonname [CVE-2023-4806 CVE-2023-5156]; reparatie van _dl_find_object om correcte waarden terug te geven, zelfs tijdens een vroege start">
<correction gosa-plugins-netgroups "Waarschuwingen over verouderd zijn weglaten in de webinterface">
<correction gosa-plugins-systems "Beheer van DHCP/DNS-vermeldingen in standaardthema verbeteren; probleem oplossen met het toevoegen van (zelfstandige) <q>netwerkprinter</q>-systemen; probleem met het genereren van doel-DN's voor verschillende systeemtypen oplossen; pictogramweergave in DHCP-servlet repareren; computernaam zonder volledige unieke domeinnaam afdwingen voor werkstations">
<correction gtk+3.0 "Nieuwe bovenstroomse stabiele release; oplossing voor verschillende crashes; meer informatie tonen in de foutopsporingsinterface <q>inspector</q>; stilleggen van GFileInfo-waarschuwingen bij gebruik met een backport-versie van GLib; een lichte kleur gebruiken voor de cursor in donkere thema's, waardoor deze in sommige apps, met name in Evince, veel gemakkelijker te zien is">
<correction gtk4 "Reparatie van tekstafbreking in de zijbalk voor plaatsnamen met de toegankelijkheidsinstelling grote tekst">
<correction haskell-hakyll "Opnieuw bouwen tegen nieuw pandoc">
<correction highway "Oplossing voor ondersteuning voor armhf-systemen zonder NEON">
<correction hnswlib "Oplossing voor dubbele vrijgave in init_index wanneer het M-argument een groot geheel getal is [CVE-2023-37365]">
<correction horizon "Probleem met open omleidingen oplossen [CVE-2022-45582]">
<correction icingaweb2 "Ongewenste meldingen over verouderd zijn onderdrukken">
<correction imlib2 "Het behoud van de alfakanaalvlag repareren">
<correction indent "Oplossing voor lezen buiten buffer; bufferoverschrijving repareren [CVE-2023-40305]">
<correction inetutils "De terugkeerwaarden controleren bij het laten vallen van privileges [CVE-2023-40303]">
<correction inn2 "Oplossing voor hangen van nnrpd wanneer compressie is ingeschakeld; ondersteuning toevoegen voor syslog-tijdstempels met hoge precisie; inn-{radius,secrets}.conf niet voor iedereen leesbaar maken">
<correction jekyll "YAML-aliassen ondersteunen">
<correction kernelshark "Segmentatiefout in libshark-tepdata repareren; vastleggen verbeteren wanneer doelmap een spatie bevat">
<correction krb5 "Probleem met het vrijgeven van een niet-geïnitialiseerde pointer oplossen [CVE-2023-36054]">
<correction lemonldap-ng "Aanmeldingscontrole toepassen op auth-slave verzoeken; open omleiding repareren als gevolg van onjuiste escape-afhandeling; open omleiding repareren wanneer OIDC RP geen omleidings-URI's heeft; probleem met vervalsing van Server Side-verzoeken oplossen [CVE-2023-44469]">
<correction libapache-mod-jk "Verwijderen van de impliciete omzettingsfunctionaliteit, die zou kunnen leiden tot onbedoelde blootstelling van de statuswerker en/of het omzeilen van beveiligingsbeperkingen [CVE-2023-41081]">
<correction libclamunrar "Nieuwe bovenstroomse stabiele release">
<correction libmatemixer "Oplossen van heap-beschadigingen/applicatiecrashes bij het verwijderen van audioapparaten">
<correction libpam-mklocaluser "pam-auth-update: ervoor zorgen dat de module vóór andere modules van het sessietype wordt gerangschikt">
<correction libxnvctrl "Nieuw bronpakket afgesplitst van nvidia-settings">
<correction linux "Nieuwe bovenstroomse stabiele release">
<correction linux-signed-amd64 "Nieuwe bovenstroomse stabiele release">
<correction linux-signed-arm64 "Nieuwe bovenstroomse stabiele release">
<correction linux-signed-i386 "Nieuwe bovenstroomse stabiele release">
<correction llvm-defaults "Reparatie van symbolische koppeling naar /usr/include/lld; Breaks toevoegen tegen pakketten die niet samen geïnstalleerd kunnen worden voor een soepelere upgrade van bullseye">
<correction ltsp "Het gebruik van mv vermijden bij symbolische koppeling naar init">
<correction lxc "Syntaxis van nftables verbeteren voor IPv6 NAT">
<correction lxcfs "CPU-rapportage verbeteren binnen een arm32-container met grote aantallen CPU's">
<correction marco "Compositie enkel inschakelen als het beschikbaar is">
<correction mariadb "Nieuwe bovenstroomse release met probleemoplossingen">
<correction mate-notification-daemon "Twee geheugenlekken repareren">
<correction mgba "Defecte audio repareren in libretro core; fcrash repareren op hardware die OpenGL 3.2 niet ondersteunt">
<correction modsecurity "Probleem van denial of service oplossen [CVE-2023-38285]">
<correction monitoring-plugins "check_disk: aankoppelen vermijden bij het zoeken naar overeenkomende koppelpunten, waarmee een snelheidsafname ten opzichte van bullseye wordt opgelost">
<correction mozjs102 "Nieuwe bovenstroomse stabiele release; oplossingem voor <q>onjuiste waarde gebruikt tijdens WASM-compilatie</q> [CVE-2023-4046], mogelijk probleem van gebruik na vrijgave [CVE-2023-37202], probleem met geheugenbeveiliging [CVE-2023-37211 CVE-2023-34416]">
<correction mutt "Nieuwe bovenstroomse stabiele release">
<correction nco "Ondersteuning voor udunits2 opnieuw inschakelen">
<correction nftables "Oplossing voor probleem van genereren van onjuiste bytecode bij een nieuwe kernelcontrole die het toevoegen van regels aan gebonden ketens weigert">
<correction node-dottie "Oplossen van veiligheidsprobleem (prototypepollutie) [CVE-2023-26132]">
<correction nvidia-settings "Nieuwe bovenstroomse release met probleemoplossingen">
<correction nvidia-settings-tesla "Nieuwe bovenstroomse release met probleemoplossingen">
<correction nx-libs "Oplossing voor ontbrekende symbolische koppeling naar /usr/share/nx/fonts; verbetering aan manpagina">
<correction open-ath9k-htc-firmware "De juiste firmware laden">
<correction openbsd-inetd "Problemen met geheugenverwerking oplossen">
<correction openrefine "Probleem met uitvoering van willekeurige code oplossen [CVE-2023-37476]">
<correction openscap "Vereisten van openscap-utils en python3-openscap oplossen">
<correction openssh "Probleem met uitvoering van externe code via een doorgestuurde agent-socket oplossen [CVE-2023-38408]">
<correction openssl "Nieuwe bovenstroomse stabiele release; oplossingen voor beveiligingsproblemen [CVE-2023-2975 CVE-2023-3446 CVE-2023-3817]">
<correction pam "Reparatie van pam-auth-update --disable; bijgewerkte Turkse vertaling">
<correction pandoc "Probleem met willekeurig schrijven in bestanden oplossen [CVE-2023-35936]">
<correction plasma-framework "Plasmashell-crashes verhelpen">
<correction plasma-workspace "Crash in krunner verhelpen">
<correction python-git "Oplossen van probleem met uitvoering van externe code [CVE-2023-40267] en van probleem met insluiting van blind lokaal bestand [CVE-2023-41040]">
<correction pywinrm "Oplossing voor compatibiliteit met Python 3.11">
<correction qemu "Bijwerken naar bovenstroomse 7.2.5-structuur; ui/vnc-clipboard: oneindige lus in inflate_buffer repareren [CVE-2023-3255]; probleem met NULL pointer dereference oplossen [CVE-2023-3354]; probleem van bufferoverloop oplossen [CVE-2023-3180]">
<correction qtlocation-opensource-src "Oplossing voor vastlopen bij het laden van kaarttegels">
<correction rar "Nieuwe bovenstroomse release met probleemoplossingen [CVE-2023-40477]">
<correction reprepro "Rasconditie verhelpen bij gebruik van externe decompressors">
<correction rmlint "Herstellen van een fout in andere pakketten veroorzaakt door een ongeldige Python-pakketversie; fout bij opstarten van GUI met recente python3.11 oplossen">
<correction roundcube "Nieuwe bovenstroomse stabiele release; repareren van OAuth2-authenticatie; problemen met cross-site scripting oplossen [CVE-2023-43770]">
<correction runit-services "dhclient: gebruik van eth1 niet vast coderen">
<correction samba "Nieuwe bovenstroomse stabiele release">
<correction sitesummary "Nieuwe bovenstroomse release; installatie van CRON/systemd-timerd-script voor sitesummary-onderhoud repareren; het onveilig aanmaken van tijdelijke bestanden en mappen repareren">
<correction slbackup-php "Probleemoplossingen: externe commando's loggen naar stderr; known hosts-bestanden van SSH uitschakelen; PHP 8 compatibiliteit">
<correction spamprobe "Crashes bij het verwerken van JPEG-bijlagen repareren">
<correction stunnel4 "Verbeterde afhandeling van een peer die een TLS-verbinding sluit zonder de juiste afsluitberichten">
<correction systemd "Nieuwe bovenstroomse stabiele release; oplossen van klein beveiligingsprobleem in arm64 en riscv64 systemd-boot (EFI) terwijl blobs voor de apparaatboom worden geladen">
<correction testng7 "Backporten naar stable voor toekomstige compilaties van openjdk-17">
<correction timg "Oplossing voor kwetsbaarheid voor bufferoverloop [CVE-2023-40968]">
<correction transmission "Vervangen van patch voor compatiliteit met openssl3 om geheugenlek te repareren">
<correction unbound "Oplossen van overstromen van foutenlogboek  bij gebruik van DNS over TLS met openssl 3.0">
<correction unrar-nonfree "Oplossen van een probleem van uitvoeren van externe code [CVE-2023-40477]">
<correction vorta "Veranderingen in ctime en mtime verwerken in diffs">
<correction vte2.91 "Ringweergave vaker ongeldig maken als dat nodig is, om verschillende bevestigingsfouten tijdens de afhandeling van gebeurtenissen op te lossen">
<correction x2goserver "x2goruncommand: ondersteuning toevoegen voor KDE Plasma 5; x2gostartagent: bederf van logbestanden voorkomen; keystrokes.cfg: synchroniseren met nx-libs; codering van Finse vertaling repareren">
</table>


<h2>Beveiligingsupdates</h2>


<p>Met deze revisie worden de volgende beveiligingsupdates toegevoegd aan de
stabiele release. Het beveiligingsteam heeft voor elk van deze updates
al een advies uitgebracht:</p>

<table border=0>
<tr><th>Advies-ID</th>  <th>Pakket</th></tr>
<dsa 2023 5454 kanboard>
<dsa 2023 5455 iperf3>
<dsa 2023 5456 chromium>
<dsa 2023 5457 webkit2gtk>
<dsa 2023 5458 openjdk-17>
<dsa 2023 5459 amd64-microcode>
<dsa 2023 5460 curl>
<dsa 2023 5462 linux-signed-amd64>
<dsa 2023 5462 linux-signed-arm64>
<dsa 2023 5462 linux-signed-i386>
<dsa 2023 5462 linux>
<dsa 2023 5463 thunderbird>
<dsa 2023 5464 firefox-esr>
<dsa 2023 5465 python-django>
<dsa 2023 5466 ntpsec>
<dsa 2023 5467 chromium>
<dsa 2023 5468 webkit2gtk>
<dsa 2023 5469 thunderbird>
<dsa 2023 5471 libhtmlcleaner-java>
<dsa 2023 5472 cjose>
<dsa 2023 5473 orthanc>
<dsa 2023 5474 intel-microcode>
<dsa 2023 5475 linux-signed-amd64>
<dsa 2023 5475 linux-signed-arm64>
<dsa 2023 5475 linux-signed-i386>
<dsa 2023 5475 linux>
<dsa 2023 5476 gst-plugins-ugly1.0>
<dsa 2023 5477 samba>
<dsa 2023 5479 chromium>
<dsa 2023 5481 fastdds>
<dsa 2023 5482 tryton-server>
<dsa 2023 5483 chromium>
<dsa 2023 5484 librsvg>
<dsa 2023 5485 firefox-esr>
<dsa 2023 5487 chromium>
<dsa 2023 5488 thunderbird>
<dsa 2023 5491 chromium>
<dsa 2023 5492 linux-signed-amd64>
<dsa 2023 5492 linux-signed-arm64>
<dsa 2023 5492 linux-signed-i386>
<dsa 2023 5492 linux>
<dsa 2023 5493 open-vm-tools>
<dsa 2023 5494 mutt>
<dsa 2023 5495 frr>
<dsa 2023 5496 firefox-esr>
<dsa 2023 5497 libwebp>
<dsa 2023 5498 thunderbird>
<dsa 2023 5501 gnome-shell>
<dsa 2023 5504 bind9>
<dsa 2023 5505 lldpd>
<dsa 2023 5507 jetty9>
<dsa 2023 5510 libvpx>
</table>


<h2>Verwijderde pakketten</h2>

<p>De volgende pakketten werden verwijderd wegens omstandigheden waarover wij geen controle hebben:</p>

<table border=0>
<tr><th>Pakket</th>               <th>Reden</th></tr>
<correction https-everywhere "verouderd, belangrijke browsers bieden er zelf ondersteuning voor">

</table>

<h2>Het Debian-installatiesysteem</h2>
<p>Het installatiesysteem werd bijgewerkt om de reparaties die met deze tussenrelease in stable, de stabiele release, opgenomen werden, toe te voegen.</p>

<h2>URL's</h2>

<p>De volledige lijsten met pakketten die met deze revisie gewijzigd werden:</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/<downcase <codename>>/ChangeLog">
</div>

<p>De huidige stabiele distributie:</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/stable/">
</div>

<p>Voorgestelde updates voor de stabiele distributie:</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/proposed-updates">
</div>

<p>informatie over de stabiele distributie (notities bij de release, errata, enz.):</p>

<div class="center">
  <a
  href="$(HOME)/releases/stable/">https://www.debian.org/releases/stable/</a>
</div>

<p>Beveiligingsaankondigingen en -informatie:</p>

<div class="center">
  <a href="$(HOME)/security/">https://www.debian.org/security/</a>
</div>

<h2>Over Debian</h2>

<p>Het Debian-project is een samenwerkingsverband van ontwikkelaars van vrije
software die vrijwillig tijd en moeite steken in het produceren van het
volledig vrije besturingssysteem Debian.</p>

<h2>Contactinformatie</h2>

<p>Ga voor verdere informatie naar de webpagina's van Debian op
<a href="$(HOME)/">https://www.debian.org/</a>, stuur een e-mail naar
&lt;press@debian.org&gt;, of neem contact met het release-team voor de stabiele
release op &lt;debian-release@lists.debian.org&gt;.</p>


