#use wml::debian::template title="Debian GNU/Hurd --- Documentatie" NOHEADER="yes"
#include "$(ENGLISHDIR)/ports/hurd/menu.inc"
#use wml::debian::translation-check translation="55a70d0c0f3df8d4df237334ac6de72daaa99f73"

<h1>Debian GNU/Hurd</h1>
<h2>Vertalers (Translators)</h2>
<ul>
<li><a href="#concept" name="TOC_concept">Concept</a></li>
<li><a href="#examples" name="TOC_examples">Voorbeelden</a></li>
<li><a href="#actpas" name="TOC_actpas">Passieve vertalers, Actieve vertalers</a></li>
<li><a href="#manage" name="TOC_manage">Vertalers beheren</a></li>
</ul>

<h3><a href="#TOC_concept" name="concept">Concept</a></h3>
<p>
Laten we, voordat we vertalers nader bekijken, eens kijken naar reguliere bestandssystemen. Een bestandssysteem is een opslagplaats voor een hiërarchische boom van mappen en bestanden. U benadert mappen en bestanden via een speciale tekenreeks, het pad. Verder zijn er symbolische koppelingen om naar één bestand te verwijzen op verschillende plaatsen in de boomstructuur, en zijn er harde koppelingen om één en hetzelfde bestand verschillende namen te geven. Er zijn ook speciale apparaatbestanden voor de communicatie met de hardware-apparaatstuurprogramma's van de kernel, en er zijn aankoppelpunten om andere opslagplaatsen in de mappenboom op te nemen. Dan zijn er nog obscure objecten zoals FIFO's.</p>
<p>
Hoewel deze objecten erg verschillend zijn, delen ze enkele gemeenschappelijke eigenschappen. Ze hebben bijvoorbeeld allemaal een eigenaar en een groep die ermee geassocieerd zijn, evenals toegangsrechten (permissies). Deze informatie wordt opgeschreven in inodes. Dit is eigenlijk nog een overeenkomst: elk object heeft precies één inode die ermee geassocieerd is (harde koppelingen zijn een beetje speciaal omdat ze één en dezelfde inode delen). Soms bevat de inode meer informatie. De inode kan bijvoorbeeld het doel van een symbolische koppeling bevatten.</p>
<p>
Deze overeenkomsten worden echter meestal niet benut in de implementaties, ondanks de gemeenschappelijke programmeerinterface. Alle inodes zijn toegankelijk via de standaard POSIX-aanroepen, bijvoorbeeld <code>read()</code> en <code>write()</code>. Om bijvoorbeeld een nieuw objecttype (bijvoorbeeld een nieuw type koppeling) toe te voegen aan een gewone monolithische Unix-kernel, zou u de code voor elk bestandssysteem afzonderlijk moeten aanpassen.</p>
<p>
In de Hurd werken de dingen anders. Hoewel in de Hurd een speciale bestandssysteemserver speciale eigenschappen van standaard objecttypes zoals koppelingen kan gebruiken (in het bestandssysteem ext2 met snelle koppelingen, bijvoorbeeld), heeft het een algemene interface om zulke eigenschappen toe te voegen zonder bestaande code aan te passen.</p>
<p>
De truc is om toe te staan dat een programma wordt ingevoegd tussen de eigenlijke inhoud van een bestand en de gebruiker die dit bestand benadert. Zo'n programma wordt een vertaler genoemd, omdat het in staat is om binnenkomende verzoeken op veel verschillende manieren te verwerken. Met andere woorden, een vertaler is een Hurd-server die de basisinterface van het bestandssysteem levert.</p>
<p>
Vertalers hebben zeer interessante eigenschappen. Vanuit het oogpunt van de kernel zijn ze gewoon een ander gebruikersproces. Dit betekent dat vertalers door iedere gebruiker uitgevoerd kunnen worden. U heeft geen systeembeheerdersrechten nodig om een vertaler te installeren of aan te passen, u heeft alleen de toegangsrechten nodig voor de onderliggende inode waaraan de vertaler gekoppeld is. Veel vertalers hebben geen echt bestand nodig om te werken, ze kunnen op eigen kracht informatie verstrekken. Dit is de reden waarom de informatie over vertalers wordt opgeslagen in de inode.</p>
<p>

Vertalers zijn verantwoordelijk voor het uitvoeren van alle bestandssysteembewerkingen die betrekking hebben op de inode waaraan ze zijn gekoppeld. Omdat ze niet beperkt zijn tot de gebruikelijke reeks objecten (apparaatbestand, koppeling, enzovoort), zijn ze vrij om alles terug te geven wat zinvol is voor de programmeur. Men zou zich een vertaler kunnen voorstellen die zich gedraagt als een map wanneer hij wordt benaderd met <code>cd</code> of <code>ls</code> en zich tegelijkertijd gedraagt als een bestand wanneer hij wordt benaderd met <code>cat</code>.</p>

<h3><a href="#TOC_examples" name="examples">Voorbeelden</a></h3>
<h4>Aankoppelpunten</h4>
<p>
Een aankoppelpunt kan gezien worden als een inode waaraan een speciale vertaler gekoppeld is. Zijn functie zou zijn om bestandssysteembewerkingen op het aankoppelpunt te vertalen in bestandssysteembewerkingen op een andere opslag, laten we zeggen, een andere partitie.</p>
<p>
Dit is inderdaad hoe bestandssystemen worden geïmplementeerd onder de Hurd. Een bestandssysteem is een vertaler. Deze vertaler neemt een opslag als argument en is in staat om alle bestandssysteembewerkingen transparant uit te voeren.</p>

<h4>Apparaatbestanden</h4>
<p>
Er zijn veel verschillende apparaatbestanden en in systemen met een monolithische kernel worden ze allemaal geleverd door de kernel zelf. In de Hurd worden alle apparaatbestanden geleverd door vertalers. Eén vertaler kan ondersteuning bieden voor veel vergelijkbare apparaatbestanden, bijvoorbeeld alle partities op de harde schijf. Op deze manier is het aantal benodigde vertalers vrij klein. Merk echter op dat voor elk apparaatbestand dat wordt geopend, een aparte vertalertaak wordt gestart. Omdat de Hurd zwaar multi-threaded is, is dit erg goedkoop.</p>
<p>
Als er hardware bij betrokken is, begint een vertaler meestal met de kernel te communiceren om de gegevens van de hardware te krijgen. Als er echter geen hardwaretoegang nodig is, hoeft de kernel er niet bij betrokken te worden. <code>/dev/zero</code> heeft bijvoorbeeld geen hardwaretoegang nodig en kan daarom volledig in de gebruikersruimte worden geïmplementeerd.</p>

<h4>Symbolische koppelingen</h4>
<p>
Een symbolische koppeling kan gezien worden als een vertaler. Het benaderen van de symbolische koppeling start de vertaler op, die het verzoek doorstuurt naar het bestandssysteem dat het bestand bevat waar de koppeling naar verwijst.</p>
<p>
Met het oog op een betere prestatie kunnen bestandssystemen met een inherente ondersteuning voor symbolische koppelingen echter voordeel halen uit deze functionaliteit en symbolische koppelingen anders implementeren. Intern zou het openen van een symbolische koppeling geen nieuw vertalerproces starten. Voor de gebruiker zou het echter nog steeds lijken alsof er een passieve vertaler bij betrokken is (zie hieronder voor een uitleg over wat een passieve vertaler is).</p>
<p>
Omdat de Hurd wordt geleverd met een vertaler voor symbolische koppelingen, heeft elke bestandssysteemserver die ondersteuning biedt voor vertalers automatisch ondersteuning voor symbolische koppelingen (en hechte koppelingen, en apparaatbestanden, enz.)! Dit betekent dat u heel snel een werkend bestandssysteem kunt krijgen en later inherente ondersteuning voor symbolische koppelingen en andere functies kunt toevoegen.</p>

<h3><a href="#TOC_actpas" name="actpas">Passieve vertalers, Actieve vertalers</a></h3>
<p>
Er zijn twee soorten vertalers, passieve en actieve. Het zijn echt totaal verschillende dingen, dus haal ze niet door elkaar, maar ze hebben een nauwe relatie met elkaar.</p>

<h4>Actieve vertalers</h4>
<p>
Een actieve vertaler is een lopend vertalerproces, zoals we er hierboven kennis mee maakten. U kunt actieve vertalers instellen en verwijderen met de opdracht <code>settrans -a</code>. De optie <code>-a</code> is nodig om <code>settrans</code> te vertellen dat u de actieve vertaler wilt wijzigen.</p>
<p>
Het commando <code>settrans</code> heeft drie soorten argumenten. Ten eerste kunt u opties instellen voor het commando <code>settrans</code> zelf, zoals <code>-a</code> om de actieve vertaler te wijzigen. Daarna stelt u de inode in die u wilt wijzigen. Onthoud dat een vertaler altijd geassocieerd is met een inode in de mappenhiërarchie. U kunt slechts één inode tegelijk wijzigen. Als u geen andere argumenten meer opgeeft, zal <code>settrans</code> proberen een bestaande vertaler te verwijderen. Hoe hard het probeert, hangt af van de forceeropties die u opgeeft (als de vertaler door een proces wordt gebruikt, krijgt u de foutmelding "device or resource busy" - "apparaat of bron is bezig", tenzij u het geforceerd laat verdwijnen).</p>
<p>
Maar als u meer argumenten opgeeft, wordt dit geïnterpreteerd als een opdrachtregel voor het uitvoeren van de vertaler. Dit betekent dat het volgende argument de bestandsnaam is van het uitvoerbare bestand van de vertaler. Verdere argumenten zijn opties voor de vertaler, en niet voor het commando <code>settrans</code>.</p>
<p>
Om bijvoorbeeld een ext2fs-partitie aan te koppelen, kunt u <code>settrans -a -c /mnt /hurd/ext2fs /dev/hd2s5</code> uitvoeren. De optie <code>-c</code> maakt het koppelpunt voor u als het nog niet bestaat. Dit hoeft overigens geen map te zijn. Om te ontkoppelen, kunt u <code>settrans -a /mnt</code> gebruiken.</p>

<h4>Passieve vertalers</h4>
<p>
Een passieve vertaler wordt ingesteld en gewijzigd met dezelfde syntaxis als een actieve vertaler (alleen de <code>-a</code> weglaten); dus alles wat hierboven is gezegd geldt ook voor passieve vertalers. Er is echter een verschil: passieve vertalers worden nog niet gestart.</p>
<p>
Dit is logisch, want dit is wat u normaal gesproken wilt. u wilt niet dat de partitie wordt aangekoppeld, tenzij u echt bestanden op deze partitie benadert. U wilt het netwerk niet opstarten tenzij er wat verkeer is, enzovoort.</p>
<p>
In plaats daarvan wordt de passieve vertaler, de eerste keer dat deze wordt benaderd, automatisch uit de inode gelezen en wordt er bovenop een actieve vertaler gestart met de opdrachtregel die was opgeslagen in de inode. Dit is vergelijkbaar met de functionaliteit van de automounter in Linux. Het is echter geen extra bonus die u handmatig moet instellen, maar een integraal onderdeel van het systeem. Het instellen van passieve vertalers stelt het starten van de vertalertaak dus uit tot u die echt nodig heeft. Trouwens, als de actieve vertaler om een of andere reden uitvalt, wordt de vertaler de volgende keer dat de inode wordt geopend, opnieuw gestart.</p>
<p>
Er is nog een verschil: actieve vertalers kunnen uitvallen of verloren gaan. Zodra het actieve vertalerproces wordt vernietigd (bijvoorbeeld omdat u de computer herstart), is het voor altijd verloren. Passieve vertalers zijn niet van voorbijgaande aard en blijven in de inode tijdens het opnieuw opstarten, totdat u ze wijzigt met het programma <code>settrans</code> of de inodes verwijdert waaraan ze zijn gekoppeld. Dit betekent dat u geen configuratiebestand met uw aankoppelpunten hoeft bij te houden.</p>
<p>
Nog een laatste punt: zelfs als u een passieve vertaler hebt ingesteld, kunt u nog steeds een andere actieve vertaler instellen. Alleen als de vertaler automatisch wordt gestart omdat er geen actieve vertaler was op het moment dat de inode werd benaderd, wordt rekening gehouden met de passieve vertaler.</p>

<h3><a href="#TOC_manage" name="manage">Vertalers beheren</a></h3>
<p>
Zoals hierboven vermeld, kun je <code>settrans</code> gebruiken om passieve en actieve vertalers in te stellen en te wijzigen. Er zijn veel opties om het gedrag van <code>settrans</code> te veranderen als er iets fout gaat, en om de actie voorwaardelijk te maken. Hier zijn enkele veelvoorkomende toepassingen:</p>
<ul><li><code>settrans -c /mnt /hurd/ext2fs /dev/hd2s5</code> koppelt een partitie aan, de vertaler blijft behouden na een herstart.</li>
<li><code>settrans -a /mnt /hurd/ext2fs ~/dummy.fs</code> koppelt een bestandssysteem aan binnen een gegevensbestand, de vertaler zal verdwijnen als deze uitvalt.</li>
<li><code>settrans -fg /nfs-data</code> dwingt een vertaler om te verdwijnen.</li>
</ul>
<p>
U kunt de opdracht <a href="hurd-doc-utils#showtrans"><code>showtrans</code></a> gebruiken om te zien of er een vertaler aan een inode is gekoppeld. Dit laat echter alleen de passieve vertaler zien.</p>
<p>
U kunt de opties van een actieve (bestandssysteem) vertaler wijzigen met <code>fsysopts</code> zonder deze daadwerkelijk opnieuw op te starten. Dit is erg handig. U kunt bijvoorbeeld doen wat onder Linux wordt genoemd "een partitie alleen-lezen opnieuw aankoppelen" door simpelweg <code>fsysopts /mntpoint --readonly</code> uit te voeren. De actieve vertaler zal indien mogelijk zijn gedrag aanpassen aan uw verzoek. <code>fsysopts /mntpoint</code> zonder een parameter toont u de huidige instellingen.</p>

<h4>Voorbeelden</h4>
<p>
Ik raad aan om te beginnen met het lezen van het commando <code>/bin/mount</code>; het is maar een klein script. Omdat het instellen van bestandssysteemvertalers vergelijkbaar is met het aankoppelen van partities, kunt u het concept op deze manier gemakkelijk begrijpen.
Maak een bestandssysteemimage met <code>dd if=/dev/zero of=dummy.fs bs=1024k count=8; mke2fs dummy.fs</code> en "koppel het aan" met <code>settrans -c dummy /hurd/ext2fs `pwd`/dummy.fs</code>. Merk op dat de vertaler nog niet wordt gestart, er loopt geen nieuw <code>ext2fs</code>-proces (verifieer met <code>ps Aux</code>). Controleer of alles klopt met <code>showtrans</code>.</p>
<p>
Typ nu <code>ls dummy</code> en u zult de korte vertraging opmerken die optreedt terwijl de vertaler wordt gestart. Daarna is er geen vertraging meer bij het benaderen van dummy. Onder Linux zou u zeggen dat u een loop-bestandssysteem automatisch hebt aangekoppeld. Controleer met <code>ps Aux</code> of er nu een <code>ext2fs dummy</code>-proces actief is. Plaats nu enkele bestanden in de nieuwe map. Probeer het bestandssysteem alleen-lezen te maken met <code>fsysopts</code>. Merk op hoe verdere schrijfpogingen nu mislukken. Probeer de actieve vertaler te vernietigen met <code>settrans -g</code>.</p>
<p>
U zou nu enigszins moeten begrijpen wat er gebeurt. Onthoud dat dit slechts <em>één</em> speciale server was, de ext2fs-server van de Hurd. Er zijn nog veel meer servers in de map <code>hurd</code>. Sommige zijn voor bestandssystemen. Sommige zijn nodig voor bestandssysteemfuncties zoals koppelingen. Sommige zijn nodig voor apparaatbestanden. Sommige zijn nuttig voor netwerken. Beeld u in dat u een FTP-server "aankoppelt" met <code>settrans</code> en bestanden eenvoudig downloadt met het standaard commando <code>cp</code>. Of dat u uw websites bewerkt met <code>emacs /ftp/startpagina.mijn.server.org/index.html</code>!</p>
