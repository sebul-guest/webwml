#use wml::debian::template title="Debian GNU/NetBSD" BARETITLE="yes" NOHEADER="yes"
#use wml::fmt::verbatim
#use wml::debian::translation-check translation="c9a7e0f78250fe2fea728e669907c9ee47374e1c"

#############################################################################
<div class="important">
<p><strong>
Deze inspanning om Debian voor deze architectuur geschikt te maken is al lang
opgegeven. Sinds oktober 2002 werd ze niet meer bijgewerkt. De informatie op
deze pagina is er enkel voor historische redenen.
</strong></p>
</div>

<h1>
Debian GNU/NetBSD
</h1>

<p>
Debian GNU/NetBSD (i386) was een streven om het Debian besturingssysteem
geschikt te maken voor de NetBSD-kernel en libc (niet te verwarren met de
andere overzettingen van Debian naar BSD, gebaseerd op glibc). Toen deze
overzetting stopgezet werd (rond oktober 2002) bevond ze
zich in een vroege ontwikkelingsfase - niettemin kon
het systeem vanuit niets geïnstalleerd worden.
</p>

<p>
Er was ook een poging om te beginnen met een overzetting naar Debian
GNU/NetBSD (alpha), welke kon worden uitgevoerd vanuit een chroot in een
systeemeigen NetBSD (alpha) systeem, maar dat niet zelfstandig kon opstarten,
en gebruik maakte van de meeste systeemeigen NetBSD-bibliotheken. Er werd een
<a href="https://lists.debian.org/debian-bsd/2002/debian-bsd-200201/msg00203.html">bericht over de stand van zaken</a> naar de lijst gestuurd.
</p>

<h2>Historisch nieuws</h2>

<dl class="gloss">
  <dt class="new">06-10-2002:</dt>
  <dd>
      Er zijn nu experimentele installatiediskettes beschikbaar voor de
      installatie van een Debian GNU/NetBSD systeem.
  </dd>
  <dt>06-03-2002:</dt>
  <dd>
      Matthew hackte <a href="https://packages.debian.org/ifupdown">ifupdown</a>
      naar a werkbare staat.
  </dd>
  <dt>25-02-2002:</dt>
  <dd>
      Matthew heeft gemeld dat ondersteuning voor shadow en PAM nu werken op
      NetBSD. <a href="https://packages.debian.org/fakeroot">fakeroot</a>
      lijkt te werken op FreeBSD, maar stelt nog steeds problemen op NetBSD.
  </dd>
  <dt>07-02-2002:</dt>
  <dd>
      Nathan heeft zopas <a
      href="https://lists.debian.org/debian-bsd/2002/debian-bsd-200202/msg00091.html">gemeld</a>
      dat hij Debian GNU/FreeBSD opgestart kreeg in multiuser modus. Hij
      werkt ook aan een installatie, enkel op basis van pakketten (met een
      gehackte debootstrap) met een aanzienlijk kleinere tarball.
  </dd>
  <dt>06-02-2002:</dt>
  <dd>
      Volgens Joel is gcc-2.95.4 geslaagd voor het grootste deel van zijn
      testsuite en werd het verpakt.
  </dd>
  <dt>06-02-2002:</dt>
  <dd>X11 werkt op NetBSD! Nogmaals een pluim voor Joel Baker
  </dd>
  <dt>04-02-2002:</dt>
  <dd>Eerste stap in de richting van een Debian/*BSD-archief: <br />
      <a href="mailto:lucifer@lightbearer.com">Joel Baker</a>
      <a href="https://lists.debian.org/debian-bsd/2002/debian-bsd-200202/msg00067.html">
      kondigde een archief aan</a> voor Debian pakketten voor FreeBSD en NetBSD
      dat geschikt is voor <kbd>dupload</kbd>.
  </dd>
  <dt>03-02-2002:</dt>
  <dd>Debian GNU/NetBSD is nu
      <a href="https://lists.debian.org/debian-bsd/2002/debian-bsd-200202/msg00043.html">
      zelf-hosting</a>! Merk op dat het voor installatie nog steeds een werkende
      NetBSD nodig heeft.
  </dd>
  <dt>30-01-2002:</dt>
  <dd>De overzetting naar Debian GNU/*BSD heeft nu een webpagina!</dd>
</dl>

<h2>Waarom Debian GNU/NetBSD?</h2>

<ul>
<li>NetBSD werkt op hardware welke niet ondersteund wordt door Linux. Debian
geschikt maken voor de NetBSD-kernel verhoogt het aantal platforms waarop het
gebruik van een op Debian gebaseerd besturingssysteem mogelijk is.</li>

<li>Het project Debian GNU/Hurd toont aan dat Debian niet gebonden is aan één
specifieke kernel. De Hurd-kernel was echter nog steeds relatief onaf - een
Debian GNU/NetBSD-systeem zou bruikbaar zijn op productieniveau.</li>

<li>Lessen die getrokken werden uit het overzetten van Debian naar NetBSD
kunnen worden gebruikt voor het geschikt maken van Debian voor andere kernels
(zoals die van <a href="https://www.freebsd.org/">FreeBSD</a>
en <a href="https://www.openbsd.org/">OpenBSD</a>).</li>

<li>In tegenstelling tot projecten zoals
<a href="https://www.finkproject.org/">Fink</a>,
was Debian GNU/NetBSD niet bedoeld om extra software of een Unix-achtige
omgeving te voorzien voor een bestaand besturingssysteem (het aantal
overzettingen van Debian naar *BSD is al uitgebreid, en deze bieden
ontegensprekelijk een Unix-achtige omgeving). In plaats daarvan zou een
gebruiker of beheerder die gewend is aan een meer traditioneel Debian-systeem
zich onmiddellijk op zijn gemak moeten voelen met een Debian GNU/NetBSD-systeem
en er zich in een relatief kort tijdsbestek bekwaam op moeten voelen.</li>

<li>Niet iedereen houdt van de *BSD-reeks omzettingen van Debian of van de
*BSD-gebruikersruimte (dit is eerder een persoonlijke voorkeur dan enige vorm
van commentaar op de kwaliteit ervan). Er zijn Linux-distributies gemaakt die
een overzetting in *BSD-stijl bieden of een gebruikersruimte in *BSD-stijl voor
diegenen die van de BSD-gebruikersomgeving houden, maar tevens de Linux-kernel
willen gebruiken - Debian GNU/NetBSD is het logische omgekeerde hiervan,
waardoor mensen die van de GNU-gebruikersruimte houden of van een pakketsysteem
in Linux-stijl, de mogelijkheid krijgen om de NetBSD-kernel te gebruiken.</li>

<li>Omdat we het kunnen.</li>
</ul>

<h2>
Bronnen
</h2>

<p>
Er is een Debian GNU/*BSD mailinglijst. De meeste van de historische
besprekingen over deze overzetting gebeurden daar en deze zijn te raadplegen in
de webarchieven op <url "https://lists.debian.org/debian-bsd/" />.
</p>

## Local variables:
## sgml-default-doctype-name: "HTML"
## End:
