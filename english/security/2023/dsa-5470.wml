<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities were discovered in python-werkzeug, a collection
of utilities for WSGI applications.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-23934">CVE-2023-23934</a>

    <p>It was discovered that Werkzeug did not properly handle the parsing
    of nameless cookies which may allow shadowing of other cookies.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-25577">CVE-2023-25577</a>

    <p>It was discovered that Werkzeug could parse unlimited number of
    parts, including file parts, which may result in denial of service.</p></li>

</ul>

<p>For the oldstable distribution (bullseye), these problems have been fixed
in version 1.0.1+dfsg1-2+deb11u1.</p>

<p>We recommend that you upgrade your python-werkzeug packages.</p>

<p>For the detailed security status of python-werkzeug please refer to its
security tracker page at:
<a href="https://security-tracker.debian.org/tracker/python-werkzeug">\
https://security-tracker.debian.org/tracker/python-werkzeug</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2023/dsa-5470.data"
# $Id: $
