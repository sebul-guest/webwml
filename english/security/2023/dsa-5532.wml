<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>Tony Battersby reported that incorrect cipher key and IV length
processing in OpenSSL, a Secure Sockets Layer toolkit, may result in
loss of confidentiality for some symmetric cipher modes.</p>

<p>Additional details can be found in the upstream advisory:
<a href="https://www.openssl.org/news/secadv/20231024.txt">\
https://www.openssl.org/news/secadv/20231024.txt</a></p>

<p>For the stable distribution (bookworm), this problem has been fixed in
version 3.0.11-1~deb12u2.</p>

<p>We recommend that you upgrade your openssl packages.</p>

<p>For the detailed security status of openssl please refer to its security
tracker page at:
<a href="https://security-tracker.debian.org/tracker/openssl">\
https://security-tracker.debian.org/tracker/openssl</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2023/dsa-5532.data"
# $Id: $
