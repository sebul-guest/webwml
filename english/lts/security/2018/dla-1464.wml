<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>An unprivileged user of dblink or postgres_fdw could bypass the checks
intended to prevent use of server-side credentials, such as a ~/.pgpass
file owned by the operating-system user running the server. Servers
allowing peer authentication on local connections are particularly
vulnerable. Other attacks such as SQL injection into a postgres_fdw
session are also possible. Attacking postgres_fdw in this way requires
the ability to create a foreign server object with selected connection
parameters, but any user with access to dblink could exploit the
problem. In general, an attacker with the ability to select the
connection parameters for a libpq-using application could cause
mischief, though other plausible attack scenarios are harder to think
of. Our thanks to Andrew Krasichkov for reporting this issue.</p>

<p>For Debian 8 <q>Jessie</q>, this problem has been fixed in version
9.4.19-0+deb8u1.</p>

<p>We recommend that you upgrade your postgresql-9.4 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1464.data"
# $Id: $
