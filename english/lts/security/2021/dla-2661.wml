<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities were discovered in jetty, a Java servlet
engine and webserver. An attacker may reveal cryptographic credentials
such as passwords to a local user, disclose installation paths, hijack
user sessions or tamper with collocated webapps.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-9735">CVE-2017-9735</a>

    <p>Jetty is prone to a timing channel in util/security/Password.java,
    which makes it easier for remote attackers to obtain access by
    observing elapsed times before rejection of incorrect passwords.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-12536">CVE-2018-12536</a>

    <p>On webapps deployed using default Error Handling, when an
    intentionally bad query arrives that doesn't match a dynamic
    url-pattern, and is eventually handled by the DefaultServlet's
    static file serving, the bad characters can trigger a
    java.nio.file.InvalidPathException which includes the full path to
    the base resource directory that the DefaultServlet and/or webapp
    is using. If this InvalidPathException is then handled by the
    default Error Handler, the InvalidPathException message is
    included in the error response, revealing the full server path to
    the requesting system.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-10241">CVE-2019-10241</a>

    <p>The server is vulnerable to XSS conditions if a remote client USES
    a specially formatted URL against the DefaultServlet or
    ResourceHandler that is configured for showing a Listing of
    directory contents.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-10247">CVE-2019-10247</a>

    <p>The server running on any OS and Jetty version combination will
    reveal the configured fully qualified directory base resource
    location on the output of the 404 error for not finding a Context
    that matches the requested path. The default server behavior on
    jetty-distribution and jetty-home will include at the end of the
    Handler tree a DefaultHandler, which is responsible for reporting
    this 404 error, it presents the various configured contexts as
    HTML for users to click through to. This produced HTML includes
    output that contains the configured fully qualified directory base
    resource location for each context.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-27216">CVE-2020-27216</a>

    <p>On Unix like systems, the system's temporary directory is shared
    between all users on that system. A collocated user can observe
    the process of creating a temporary sub directory in the shared
    temporary directory and race to complete the creation of the
    temporary subdirectory. If the attacker wins the race then they
    will have read and write permission to the subdirectory used to
    unpack web applications, including their WEB-INF/lib jar files and
    JSP files. If any code is ever executed out of this temporary
    directory, this can lead to a local privilege escalation
    vulnerability.</p></li>

</ul>

<p>This update also includes several other bug fixes and
improvements. For more information please refer to the upstream
changelog file.</p>

<p>For Debian 9 stretch, these problems have been fixed in version
9.2.30-0+deb9u1.</p>

<p>We recommend that you upgrade your jetty9 packages.</p>

<p>For the detailed security status of jetty9 please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/jetty9">https://security-tracker.debian.org/tracker/jetty9</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2661.data"
# $Id: $
