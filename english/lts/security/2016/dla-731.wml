<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Several issues have been discovered in ImageMagick, a popular set of
programs and libraries for image manipulation. These issues include
several problems in memory handling that can result in a denial of
service attack or in execution of arbitrary code by an attacker with
control on the image input.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
8:6.7.7.10-5+deb7u8.</p>

<p>We recommend that you upgrade your imagemagick packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-731.data"
# $Id: $
