<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>This update adds size checks to thumbnail extraction. Prior to these checks, it
was possible to overflow arguments to e.g. malloc and thus cause out-of-bounds
memory accesses.</p>

<p>For Debian 10 buster, this problem has been fixed in version
0.19.2-2+deb10u2.</p>

<p>We recommend that you upgrade your libraw packages.</p>

<p>For the detailed security status of libraw please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/libraw">https://security-tracker.debian.org/tracker/libraw</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3214.data"
# $Id: $
