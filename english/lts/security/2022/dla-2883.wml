<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>It was discovered that there were two "invalid free" issues in uriparser, a
C library for parsing URLs according to RFC 3986.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-46141">CVE-2021-46141</a>

    <p>An issue was discovered in uriparser before 0.9.6. It performs invalid
    free operations in uriFreeUriMembers and uriMakeOwner.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-46142">CVE-2021-46142</a>

    <p>An issue was discovered in uriparser before 0.9.6. It performs invalid
    free operations in uriNormalizeSyntax.</p></li>

</ul>

<p>For Debian 9 <q>Stretch</q>, these problems have been fixed in version
0.8.4-1+deb9u3.</p>

<p>We recommend that you upgrade your  packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-2883.data"
# $Id: $
