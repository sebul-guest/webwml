<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>It was discovered that there was a potential directory traversal
vulnerablilty in ruby-tzinfo, a timezone library for the Ruby
programming language.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-31163">CVE-2022-31163</a>

    <p>TZInfo is a Ruby library that provides access to time zone
    data and allows times to be converted using time zone rules.
    Versions prior to 0.36.1, as well as those prior to 1.2.10 when
    used with the Ruby data source tzinfo-data, are vulnerable to
    relative path traversal. With the Ruby data source, time zones
    are defined in Ruby files. There is one file per time zone. Time
    zone files are loaded with `require` on demand. In the affected
    versions, `TZInfo::Timezone.get` fails to validate time zone
    identifiers correctly, allowing a new line character within the
    identifier. With Ruby version 1.9.3 and later,
    `TZInfo::Timezone.get` can be made to load unintended files with
    `require`, executing them within the Ruby process. Versions
    0.3.61 and 1.2.10 include fixes to correctly validate time zone
    identifiers. Versions 2.0.0 and later are not vulnerable. Version
    0.3.61 can still load arbitrary files from the Ruby load path if
    their name follows the rules for a valid time zone identifier and
    the file has a prefix of `tzinfo/definition` within a directory
    in the load path. Applications should ensure that untrusted files
    are not placed in a directory on the load path. As a workaround,
    the time zone identifier can be validated before passing to
    `TZInfo::Timezone.get` by ensuring it matches the regular
    expression
    `\A[A-Za-z0-9+\-_]+(?:\/[A-Za-z0-9+\-_]+)*\z`.</p></li>

</ul>

<p>For Debian 10 <q>Buster</q>, these problems have been fixed in version
1.2.5-1+deb10u1.</p>

<p>We recommend that you upgrade your ruby-tzinfo packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3077.data"
# $Id: $
