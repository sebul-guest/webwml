<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Jason A. Donenfeld found an ansi escape sequence injection into
software-properties, a manager for apt repository sources. An attacker
could manipulate the screen of a user prompted to install an
additional repository (PPA).</p>

<p>For Debian 9 stretch, this problem has been fixed in version
0.96.20.2-1+deb9u1.</p>

<p>We recommend that you upgrade your software-properties packages.</p>

<p>For the detailed security status of software-properties please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/software-properties">https://security-tracker.debian.org/tracker/software-properties</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2339.data"
# $Id: $
