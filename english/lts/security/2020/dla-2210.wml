<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>When normalizing ar member names by removing trailing whitespace
and slashes, an out-out-bound read can be caused if the ar member
name consists only of such characters, because the code did not
stop at 0, but would wrap around and continue reading from the
stack, without any limit.</p>

<p>For Debian 8 <q>Jessie</q>, this problem has been fixed in version
1.0.9.8.6.</p>

<p>We recommend that you upgrade your apt packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2210.data"
# $Id: $
