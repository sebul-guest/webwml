<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Multiple security issues have been found in the Mozilla Firefox web
browser, which could potentially result in the execution of arbitrary
code.</p>

<p>Debian follows the extended support releases (ESR) of Firefox. Support
for the 102.x series has ended, so starting with this update we're now
following the 115.x releases.</p>

<p>Between 102.x and 115.x, Firefox has seen a number of feature updates.
For more information please refer to
<a href="https://www.mozilla.org/en-US/firefox/115.0esr/releasenotes/">https://www.mozilla.org/en-US/firefox/115.0esr/releasenotes/</a></p>

<p>For Debian 10 buster, these problems have been fixed in version
115.3.0esr-1~deb10u1.</p>

<p>We recommend that you upgrade your firefox-esr packages.</p>

<p>For the detailed security status of firefox-esr please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/firefox-esr">https://security-tracker.debian.org/tracker/firefox-esr</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3587.data"
# $Id: $
