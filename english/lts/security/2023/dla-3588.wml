<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Multiple vulnerabilities were found in vim a text editor.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-4752">CVE-2023-4752</a>

    <p>A heap use after free was found in ins_compl_get_exp()</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-4781">CVE-2023-4781</a>

    <p>A heap-buffer-overflow was found in vim_regsub_both()</p></li>

</ul>

<p>For Debian 10 buster, these problems have been fixed in version
2:8.1.0875-5+deb10u6.</p>

<p>We recommend that you upgrade your vim packages.</p>

<p>For the detailed security status of vim please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/vim">https://security-tracker.debian.org/tracker/vim</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3588.data"
# $Id: $
