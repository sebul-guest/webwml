<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>The recent update of jetty9, released as DLA 3641-1, caused a regression in
PuppetDB, a major component of Puppet that helps you manage and automate the
configuration of servers. More specifically another package, trapperkeeperwebserver-jetty9-clojure, still used the deprecated SslContextFactory class
which made PuppetDB fail to start. This update makes use of the preferred new
SslContextFactory#Server class now.</p>

<p>For Debian 10 buster, this problem has been fixed in version
1.7.0-2+deb10u2.</p>

<p>We recommend that you upgrade your trapperkeeper-webserver-jetty9-clojure
packages.</p>

<p>For the detailed security status of trapperkeeper-webserver-jetty9-clojure
please refer to
its security tracker page at:
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/trapperkeeper-webserver-jetty9-clojure">https://security-tracker.debian.org/tracker/trapperkeeper-webserver-jetty9-clojure</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3647.data"
# $Id: $
