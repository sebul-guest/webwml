<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>A potential Cross Site Scripting (XSS) vulnerablity (<a href="https://security-tracker.debian.org/tracker/CVE-2022-36180">CVE-2022-36180</a>) and
session handling vulnerability (<a href="https://security-tracker.debian.org/tracker/CVE-2022-36179">CVE-2022-36179</a> )have been found in
fusiondirectory, a Web Based LDAP Administration Program.</p>

<p>Additionally, fusiondirectory has been updated to address the API change
in php-cas due to <a href="https://security-tracker.debian.org/tracker/CVE-2022-39369">CVE-2022-39369</a>. see DLA 3485-1Add for details.</p>

<p>Due to this, if CAS authentication is used, fusiondirectory
will stop working until those steps are done:</p>

<p>- make sure to install the updated fusiondirectory-schema package for
  buster.</p>

<p>- update the fusiondirectory core schema in LDAP by running
    fusiondirectory-insert-schema -m</p>

<p>- switch to using the new php-cas API by running
    fusiondirectory-setup --set-config-CasLibraryBool=TRUE</p>

<p>- set the CAS ClientServiceName to the base URL of the fusiondirectory
  installation, for example:
    fusiondirectory-setup --set-config-CasClientServiceName="https://fusiondirectory.example.org/"</p>


<p>For Debian 10 buster, these problems have been fixed in version
1.2.3-4+deb10u2.</p>

<p>We recommend that you upgrade your fusiondirectory packages.</p>

<p>For the detailed security status of fusiondirectory please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/fusiondirectory">https://security-tracker.debian.org/tracker/fusiondirectory</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3487.data"
# $Id: $
