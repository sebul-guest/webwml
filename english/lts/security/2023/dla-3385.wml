<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities were discovered in Apache Traffic Server, a reverse
and forward proxy server, which could result in HTTP request smuggling, cache
poisoning or information disclosure.</p>

<p>For Debian 10 buster, these problems have been fixed in version
8.1.6+ds-1~deb10u1.</p>

<p>We recommend that you upgrade your trafficserver packages.</p>

<p>For the detailed security status of trafficserver please refer to
its security tracker page at:
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/trafficserver">https://security-tracker.debian.org/tracker/trafficserver</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3385.data"
# $Id: $
