<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>It was discovered that there was an issue in the opendmarc DMARC email
filter system. A call to <tt>db_stop</tt> was missing from the
post-installation script which meant that, under some configurations, the
script would hang indefinitely.</p>

<p>For Debian 10 <q>Buster</q>, this problem has been fixed in version
1.3.2-6+deb10u4.</p>

<p>We recommend that you upgrade your opendmarc packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3550.data"
# $Id: $
