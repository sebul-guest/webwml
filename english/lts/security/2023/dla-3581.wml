<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>A buffer overflow vulnerability was found in FLAC, a free lossless audio
codec, in the bitwriter_grow_ function.  This flaw may allow remote
attackers to run arbitrary code via specially crafted input to the
encoder.</p>

<p>For Debian 10 buster, this problem has been fixed in version
1.3.2-3+deb10u3.</p>

<p>We recommend that you upgrade your flac packages.</p>

<p>For the detailed security status of flac please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/flac">https://security-tracker.debian.org/tracker/flac</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3581.data"
# $Id: $
