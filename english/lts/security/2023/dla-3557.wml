<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>It was discovered that there was a potential Denial of Service (DoS)
vulnerability in memcached, a high-performance in-memory object caching
system.</p>

<p>A crash could have occured when handling "multi-packet" uploads in UDP mode.
Deployments of memcached that only use TCP are likely unaffected by this
issue.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-48571">CVE-2022-48571</a>

    <p>memcached 1.6.7 allows a Denial of Service via multi-packet uploads in UDP.</p></li>

</ul>

<p>For Debian 10 <q>Buster</q>, this problem has been fixed in version
1.5.6-1.1+deb10u1.</p>

<p>We recommend that you upgrade your memcached packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3557.data"
# $Id: $
