<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Vulnerabilities have been found in Node.js, which could result in DNS
rebinding or arbitrary code execution.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-43548">CVE-2022-43548</a>

    <p>The Node.js rebinding protector for <code>--inspect</code> still allows invalid
    IP addresses, specifically in octal format, which browsers such as
    Firefox attempt to resolve via DNS.  When combined with an active
    <code>--inspect</code> session, such as when using VSCode, an attacker can
    perform DNS rebinding and execute arbitrary code.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-23920">CVE-2023-23920</a>

    <p>Ben Noordhuis reported that Node.js would search and potentially
    load ICU data when running with elevated privileges.  Node.js now
    builds with <code>ICU_NO_USER_DATA_OVERRIDE</code> to avoid this.</p></li>

</ul>

<p>For Debian 10 buster, these problems have been fixed in version
10.24.0~dfsg-1~deb10u3.</p>

<p>We recommend that you upgrade your nodejs packages.</p>

<p>For the detailed security status of nodejs please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/nodejs">https://security-tracker.debian.org/tracker/nodejs</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3344.data"
# $Id: $
