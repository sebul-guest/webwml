<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Two vulnerabilities have been fixed in poppler,   
a PDF rendering library.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-36023">CVE-2020-36023</a>

    <p>Infinite loop in FoFiType1C::cvtGlyph()</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-36024">CVE-2020-36024</a>

    <p>NULL dereference in FoFiType1C::convertToType1()</p></li>

</ul>

<p>For Debian 10 buster, these problems have been fixed in version
0.71.0-5+deb10u2.</p>

<p>We recommend that you upgrade your poppler packages.</p>

<p>For the detailed security status of poppler please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/poppler">https://security-tracker.debian.org/tracker/poppler</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3528.data"
# $Id: $
