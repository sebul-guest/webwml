<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Denys Klymenko discovered a cross-site scripting (XSS) vulnerability in
Roundcube, a skinnable AJAX based webmail solution for IMAP servers,
which could allow a remote attacker to load arbitrary JavaScript code
via a malicious text/html e-mail message with a crafted SVG document.</p>

<p>The issue is due to an incomplete fix for <a href="https://security-tracker.debian.org/tracker/CVE-2020-16145">CVE-2020-16145</a></p>

<p>For Debian 10 buster, this problem has been fixed in version
1.3.17+dfsg.1-1~deb10u4.</p>

<p>We recommend that you upgrade your roundcube packages.</p>

<p>For the detailed security status of roundcube please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/roundcube">https://security-tracker.debian.org/tracker/roundcube</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3630.data"
# $Id: $
