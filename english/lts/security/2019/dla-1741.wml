<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Several vulnerabilities have been found in php5, a server-side,
HTML-embedded scripting language.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-9637">CVE-2019-9637</a>

      <p>rename() across the device may allow unwanted access during
      processing.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-9638">CVE-2019-9638</a>
    <a href="https://security-tracker.debian.org/tracker/CVE-2019-9639">CVE-2019-9639</a>
      <p>Uninitialized read in exif_process_IFD_in_MAKERNOTE.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-9640">CVE-2019-9640</a>

      <p>Invalid Read on exif_process_SOFn.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-9641">CVE-2019-9641</a>

      <p>Uninitialized read in exif_process_IFD_in_TIFF.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-9022">CVE-2019-9022</a>

      <p>An issue during parsing of DNS responses allows a hostile DNS server
      to misuse memcpy, which leads to a read operation past an allocated
      buffer.</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
5.6.40+dfsg-0+deb8u2.</p>

<p>We recommend that you upgrade your php5 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>

</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1741.data"
# $Id: $
