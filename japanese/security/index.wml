#use wml::debian::template title="セキュリティ情報" GEN_TIME="yes" MAINPAGE="true"
#use wml::debian::toc
#use wml::debian::recent_list_security
#use wml::debian::translation-check translation="8b50567d4a63a6d26eb1f8b44b53c5e3678a31c0"
#include "$(ENGLISHDIR)/releases/info"

<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">

<ul class="toc">
<li><a href="#keeping-secure">あなたの Debian システムのセキュリティを維持するには</a></li>
<li><a href="#DSAS">最近の勧告</a></li>
</ul>

<aside>
<p><span class="fas fa-caret-right fa-3x"></span>Debian ではセキュリティを非常に深刻に捉えています。
私たちは気づいた問題のすべてを取り扱い、適切な期間内に修正することを
保証しています。
</aside>

<p><q>隠蔽によるセキュリティ</q>はうまくいかないことが経験的にわかっています。
逆に、セキュリティ問題を公表することにより、その問題をより迅速でより
良く解決することが可能になります。このような観点から、このページでは 
Debian の様々な既知のセキュリティホールの状態を示しています。これは潜在
的に Debian に影響を及ぼすかもしれません。</p>

<p>
Debianプロジェクトは多くのセキュリティ勧告を公表するにあたり、他のフリーソフトウェア開発元と協調しています。
その結果、これらの勧告は脆弱性が公開された同日にだされています。
</p>

# "reasonable timeframe" might be too vague, but we don't have 
# accurate statistics. For older (out of date) information and data
# please read:
# https://www.debian.org/News/2004/20040406  [ Year 2004 data ]
# and (older)
# https://people.debian.org/~jfs/debconf3/security/ [ Year 2003 data ]
# https://lists.debian.org/debian-security/2001/12/msg00257.html [ Year 2001]
# If anyone wants to do up-to-date analysis please contact me (jfs)
# and I will provide scripts, data and database schemas.

<p>Debian はセキュリティの標準化努力にも参加しています。</p>
<ul>
<li><a href="#DSAS">Debian セキュリティ勧告</a>は <a href="cve-compatibility">CVE と互換性があります</a> (<a href="crossreferences">クロスリファレンス</a>を見てください)</li>
<li>Debianは、<a href="https://oval.cisecurity.org/">Open Vulnerability Assessment Language</a>プロジェクトの理事も務めています</li>
</ul>

<h2><a id="keeping-secure">あなたの Debian システムのセキュリティを維持するには</a></h2>

<p>最新の Debian セキュリティ勧告を受け取るには、
<a href="https://lists.debian.org/debian-security-announce/">\
debian-security-announce</a>
メーリングリストを講読してください。</p>

<p><a href="https://packages.debian.org/stable/admin/apt">apt</a>
を使うと、簡単に最新のセキュリティアップデートを取得することができます。
こちらを利用するためにはご自分の <code>/etc/apt/sources.list</code> ファイルに
</p>

<pre>
deb http://security.debian.org/debian-security <current_release_security_name> main contrib non-free non-free-firmware
</pre>

<p>
という行が必要になります。それから次の2つのコマンドを実行して未処理の更新をダウンロード、適用してください。
</p>

<pre>
apt-get update &amp;&amp; apt-get upgrade
</pre>

<p>
セキュリティアーカイブは通常の Debian アーカイブ<a
href="https://ftp-master.debian.org/keys.html">署名用の鍵</a>で署名されています。
</p>

<p>
Debian のセキュリティについてもっと知るためには、FAQとマニュアルを参照してください。
</p>

<p style="text-align:center"><button type="button"><span class="fas fa-book-open fa-2x"></span> <a href="faq">セキュリティー FAQ</a></button> <button type="button"><span class="fas fa-book-open fa-2x"></span> <a href="../doc/user-manuals#securing">Debian セキュリティ強化</a></button></p>

<aside class="light">
  <span class="fa fa-rss fa-5x"></span>
</aside>

<h2><a id="DSAS">最近の勧告</a></h2>

<p>以下のページは、
<a href="https://lists.debian.org/debian-security-announce/">\
debian-security-announce</a> メーリングリストに投稿された
セキュリティ勧告をまとめたものです。

<p>
<:= get_recent_security_list( '1m', '6', '.', '$(ENGLISHDIR)/security' ) :>
</p>

{#rss#:
<link rel="alternate" type="application/rss+xml"
 title="Debian セキュリティ勧告 (タイトルのみ)" href="dsa">
<link rel="alternate" type="application/rss+xml"
 title="Debian セキュリティ勧告 (サマリー)" href="dsa-long">
:#rss#}

<p>最新の Debian セキュリティ勧告は、
<a href="dsa">RDF フォーマット</a>でも利用できます。</p>
また、何についての勧告かがわかるように、対応する勧告の最初の段落を含む<a
href="dsa-long">もうひとつのファイル</a>も提供しています。

#include "$(ENGLISHDIR)/security/index.include"
<p>古いセキュリティ勧告は、以下で見ることができます。
<:= get_past_sec_list(); :>

<p>Debian ディストリビューションはどのセキュリティ問題に対しても脆弱ではありません。
<a href="https://security-tracker.debian.org/">Debian Security Tracker</a> 
は Debian パッケージの脆弱性の状態についてのすべての情報を収集しており、
CVE 名またはパッケージ名で検索できます。</p>
