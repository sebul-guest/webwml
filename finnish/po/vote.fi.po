msgid ""
msgstr ""
"Project-Id-Version: debian-webwml\n"
"PO-Revision-Date: 2016-05-22 22:29+0300\n"
"Last-Translator: Tommi Vainikainen <thv+debian@iki.fi>\n"
"Language-Team: Finnish <debian-l10n-finnish@lists.debian.org>\n"
"Language: fi\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../english/template/debian/votebar.wml:13
msgid "Date"
msgstr "Päiväys"

#: ../../english/template/debian/votebar.wml:16
msgid "Time Line"
msgstr "Aikajana"

#: ../../english/template/debian/votebar.wml:19
msgid "Summary"
msgstr "Tiivistelmä"

#: ../../english/template/debian/votebar.wml:22
msgid "Nominations"
msgstr "Nimeämiset"

#: ../../english/template/debian/votebar.wml:25
#, fuzzy
msgid "Withdrawals"
msgstr "Vedetty pois"

#: ../../english/template/debian/votebar.wml:28
msgid "Debate"
msgstr "Väittely"

#: ../../english/template/debian/votebar.wml:31
msgid "Platforms"
msgstr "Vaaliohjelmat"

#: ../../english/template/debian/votebar.wml:34
msgid "Proposer"
msgstr "Ehdottaja"

#: ../../english/template/debian/votebar.wml:37
msgid "Proposal A Proposer"
msgstr "Ehdotuksen A ehdottaja"

#: ../../english/template/debian/votebar.wml:40
msgid "Proposal B Proposer"
msgstr "Ehdotuksen B ehdottaja"

#: ../../english/template/debian/votebar.wml:43
msgid "Proposal C Proposer"
msgstr "Ehdotuksen C ehdottaja"

#: ../../english/template/debian/votebar.wml:46
msgid "Proposal D Proposer"
msgstr "Ehdotuksen D ehdottaja"

#: ../../english/template/debian/votebar.wml:49
msgid "Proposal E Proposer"
msgstr "Ehdotuksen E ehdottaja"

#: ../../english/template/debian/votebar.wml:52
msgid "Proposal F Proposer"
msgstr "Ehdotuksen F ehdottaja"

#: ../../english/template/debian/votebar.wml:55
#, fuzzy
msgid "Proposal G Proposer"
msgstr "Ehdotuksen A ehdottaja"

#: ../../english/template/debian/votebar.wml:58
#, fuzzy
msgid "Proposal H Proposer"
msgstr "Ehdotuksen A ehdottaja"

#: ../../english/template/debian/votebar.wml:61
msgid "Seconds"
msgstr "Kannatukset"

#: ../../english/template/debian/votebar.wml:64
msgid "Proposal A Seconds"
msgstr "Ehdotuksen A kannattajat"

#: ../../english/template/debian/votebar.wml:67
msgid "Proposal B Seconds"
msgstr "Ehdotuksen B kannattajat"

#: ../../english/template/debian/votebar.wml:70
msgid "Proposal C Seconds"
msgstr "Ehdotuksen C kannattajat"

#: ../../english/template/debian/votebar.wml:73
msgid "Proposal D Seconds"
msgstr "Ehdotuksen D kannattajat"

#: ../../english/template/debian/votebar.wml:76
msgid "Proposal E Seconds"
msgstr "Ehdotuksen E kannattajat"

#: ../../english/template/debian/votebar.wml:79
msgid "Proposal F Seconds"
msgstr "Ehdotuksen F kannattajat"

#: ../../english/template/debian/votebar.wml:82
#, fuzzy
msgid "Proposal G Seconds"
msgstr "Ehdotuksen A kannattajat"

#: ../../english/template/debian/votebar.wml:85
#, fuzzy
msgid "Proposal H Seconds"
msgstr "Ehdotuksen A kannattajat"

#: ../../english/template/debian/votebar.wml:88
msgid "Opposition"
msgstr "Vastustus"

#: ../../english/template/debian/votebar.wml:91
msgid "Text"
msgstr "Teksti"

#: ../../english/template/debian/votebar.wml:94
msgid "Proposal A"
msgstr "Ehdotus A"

#: ../../english/template/debian/votebar.wml:97
msgid "Proposal B"
msgstr "Ehdotus B"

#: ../../english/template/debian/votebar.wml:100
msgid "Proposal C"
msgstr "Ehdotus C"

#: ../../english/template/debian/votebar.wml:103
msgid "Proposal D"
msgstr "Ehdotus D"

#: ../../english/template/debian/votebar.wml:106
msgid "Proposal E"
msgstr "Ehdotus E"

#: ../../english/template/debian/votebar.wml:109
msgid "Proposal F"
msgstr "Ehdotus F"

#: ../../english/template/debian/votebar.wml:112
#, fuzzy
msgid "Proposal G"
msgstr "Ehdotus A"

#: ../../english/template/debian/votebar.wml:115
#, fuzzy
msgid "Proposal H"
msgstr "Ehdotus A"

#: ../../english/template/debian/votebar.wml:118
msgid "Choices"
msgstr "Vaihtoehdot"

#: ../../english/template/debian/votebar.wml:121
msgid "Amendment Proposer"
msgstr "Muutosehdotuksen tekijä"

#: ../../english/template/debian/votebar.wml:124
msgid "Amendment Seconds"
msgstr "Muutosehdotuksen kannattajat"

#: ../../english/template/debian/votebar.wml:127
msgid "Amendment Text"
msgstr "Muutosehdotuksen teksti"

#: ../../english/template/debian/votebar.wml:130
msgid "Amendment Proposer A"
msgstr "Muutosehdotuksen A tekijä"

#: ../../english/template/debian/votebar.wml:133
msgid "Amendment Seconds A"
msgstr "Muutosehdotuksen A kannattajat"

#: ../../english/template/debian/votebar.wml:136
msgid "Amendment Text A"
msgstr "Muutosehdotuksen A teksti"

#: ../../english/template/debian/votebar.wml:139
msgid "Amendment Proposer B"
msgstr "Muutosehdotuksen B tekijä"

#: ../../english/template/debian/votebar.wml:142
msgid "Amendment Seconds B"
msgstr "Muutosehdotuksen B kannattajat"

#: ../../english/template/debian/votebar.wml:145
msgid "Amendment Text B"
msgstr "Muutosehdotuksen B teksti"

#: ../../english/template/debian/votebar.wml:148
msgid "Amendment Proposer C"
msgstr "Muutosehdotuksen C tekijä"

#: ../../english/template/debian/votebar.wml:151
msgid "Amendment Seconds C"
msgstr "Muutosehdotuksen C kannattajat"

#: ../../english/template/debian/votebar.wml:154
msgid "Amendment Text C"
msgstr "Muutosehdotuksen C teksti"

#: ../../english/template/debian/votebar.wml:157
msgid "Amendments"
msgstr "Muutosehdotukset"

#: ../../english/template/debian/votebar.wml:160
msgid "Proceedings"
msgstr "Pöytäkirjat"

#: ../../english/template/debian/votebar.wml:163
msgid "Majority Requirement"
msgstr "Enemmistövaatimus"

#: ../../english/template/debian/votebar.wml:166
msgid "Data and Statistics"
msgstr "Tiedot ja tilastot"

#: ../../english/template/debian/votebar.wml:169
msgid "Quorum"
msgstr "Määrävaatimus"

#: ../../english/template/debian/votebar.wml:172
msgid "Minimum Discussion"
msgstr "Vähimmäismäärä keskustelua"

#: ../../english/template/debian/votebar.wml:175
msgid "Ballot"
msgstr "Äänestyslippu"

#: ../../english/template/debian/votebar.wml:178
msgid "Forum"
msgstr "Foorumi"

#: ../../english/template/debian/votebar.wml:181
msgid "Outcome"
msgstr "Tulos"

#: ../../english/template/debian/votebar.wml:185
msgid "Waiting&nbsp;for&nbsp;Sponsors"
msgstr "Kannatusta odottavat"

#: ../../english/template/debian/votebar.wml:188
msgid "In&nbsp;Discussion"
msgstr "Keskusteltavana"

#: ../../english/template/debian/votebar.wml:191
msgid "Voting&nbsp;Open"
msgstr "Äänestys&nbsp;käynnissä"

#: ../../english/template/debian/votebar.wml:194
msgid "Decided"
msgstr "Päätetty"

#: ../../english/template/debian/votebar.wml:197
msgid "Withdrawn"
msgstr "Vedetty pois"

#: ../../english/template/debian/votebar.wml:200
msgid "Other"
msgstr "Muu"

#: ../../english/template/debian/votebar.wml:204
msgid "Home&nbsp;Vote&nbsp;Page"
msgstr "Koti&nbsp;äänestyssivu"

#: ../../english/template/debian/votebar.wml:207
msgid "How&nbsp;To"
msgstr "Kuinka"

#: ../../english/template/debian/votebar.wml:210
msgid "Submit&nbsp;a&nbsp;Proposal"
msgstr "Ehdottaa"

#: ../../english/template/debian/votebar.wml:213
msgid "Amend&nbsp;a&nbsp;Proposal"
msgstr "Täydentää&nbsp;ehdotusta"

#: ../../english/template/debian/votebar.wml:216
msgid "Follow&nbsp;a&nbsp;Proposal"
msgstr "Seurata&nbsp;ehdotuksen&nbsp;kehitystä"

#: ../../english/template/debian/votebar.wml:219
msgid "Read&nbsp;a&nbsp;Result"
msgstr "Lukea&nbsp;tulos"

#: ../../english/template/debian/votebar.wml:222
msgid "Vote"
msgstr "Äänestää"
